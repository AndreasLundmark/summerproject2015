//Info.cpp
//F�r att f�ra �ver v�rden mellan olika states s� h�ller Info klassen i v�rdena.
#include <iostream>
#include <fstream>
#include <string>
#include "stdafx.h"
#include "Info.h"

Info::Info()
{
}

Info::~Info()
{
}

bool Info::Initialize()
{
	return true;
}

int Info::ReadFile(int getline, std::string file) // parts of code from http://www.cplusplus.com/forum/beginner/8388/
{
	std::string line;
	std::ifstream myfile;
	myfile.open(file);

	if (myfile.is_open())
	{
		for (int i = 0; i < getline; i++) // k�r igenom filen till �nskad rad
		{
			std::getline(myfile, line);
		}
		myfile.close();
	}
	else
	{
		printf("unable to open ", myfile);
	}
	return atoi(line.c_str());
}

void Info::SaveToFile(int players, int tower, int shopitem) // save to the file
{
	std::ofstream myfile;
	myfile.open("../conf/gso.txt");
	myfile << players << "\n";
	myfile << tower << "\n";
	if (shopitem !=0)
		myfile << shopitem << "\n";
	myfile.close();
}

void Info::GoldSaveToFile(int value) // save to the file
{
	std::string line;
	std::ifstream myfile;
	myfile.open("../conf/pg.txt");

	if (myfile.is_open())
	{
		for (int i = 0; i < 1; i++) // k�r igenom filen till �nskad rad
		{
			std::getline(myfile, line);
		}
		myfile.close();
	}

	std::ofstream myfile1;
	myfile1.open("../conf/pg.txt");
	myfile1 << atoi(line.c_str()) + value << "\n";
	myfile1.close();
}

void Info::GoldMenuSaveToFile(int value) // save to the file
{
	std::ofstream myfile;
	myfile.open("../conf/pg.txt");
	myfile << value << "\n";
	myfile.close();
}


