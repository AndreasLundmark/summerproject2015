//Enemy.cpp

#include "stdafx.h"
#include "ServiceLocator.hpp"
#include "TextureManager.hpp"
#include "DrawManager.hpp"
#include "Tower.h"
#include "Enemy.h"

#include "EnemySpawner.h"
#include "Randomizer.h"
#include "Collider.h"

EnemySpawner::EnemySpawner(Tower* tower, Enemy* enemy)
{
	p_tower = tower;
	p_enemy = enemy;

	TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();
	m_EnemySpawnerSprite.setTexture(*texture_manager->LoadTexture("../assets/greencube.png"));
	m_EnemySpawnerSprite.setTextureRect(sf::IntRect(0, 0, 0, 0));

	//Starting point
	m_x = 0.0f;
	m_y = 0.0f;

	//Initialize collider for the whole player sprite
	p_collider = new Collider();
	p_collider->SetWidthHeight(m_x,m_y);

	m_enemyAmount = 0;
	m_enemyType = 1;
	m_spawnCooldown = 0;

	//Set visible
	m_visible = true;
}

EnemySpawner::~EnemySpawner()
{
	if (p_collider)
	{
		delete p_collider;
		p_collider = nullptr;
	}
}

void EnemySpawner::Update(float deltaTime)
{
	if (m_spawnCooldown <= 0 && p_tower->GetBossDoorReadyBool() == false)
	{
		m_spawnCooldown = p_random->GetRandomIntBetween(500,800);
		m_enemyAmount = p_random->GetRandomInt(2);
		m_enemyType = 1;
		//printf("%d", m_spawnCooldown);
	}
	if (m_spawnCooldown > 0 && p_tower->GetMoveBool() == true)
	{
		m_spawnCooldown -= 2;
		//printf("%d", m_spawnCooldown);
	}
	if (m_spawnCooldown > 0 && p_tower->GetMoveBool() == false)
	{
		m_spawnCooldown -= 1;
		//printf("%d", m_spawnCooldown);
	}
}

void EnemySpawner::Spawn()
{
	m_enemyAmount = 1;
}

Collider* EnemySpawner::GetCollider()
{
	return p_collider;
}

float EnemySpawner::GetX()
{
	return m_x;
}

float EnemySpawner::GetY()
{
	return m_y;
}

bool EnemySpawner::IsVisible()
{
	return m_visible;
}

EEntityType EnemySpawner::GetType()
{
	return ENTITY_ENEMYSPAWNER;
}


void EnemySpawner::SetAlive(bool mode)
{
	m_visible = mode;
}

sf::Vector2f EnemySpawner::GetPos()
{
	return m_EnemySpawnerSprite.getPosition();
}

sf::Sprite EnemySpawner::GetSprite()
{
	return m_EnemySpawnerSprite;
}

int EnemySpawner::GetEnemyAmount()
{
	return m_enemyAmount;
}

int EnemySpawner::GetSpawnCooldown()
{
	return m_spawnCooldown;
}
