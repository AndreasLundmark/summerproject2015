// Engine.cpp

#include "stdafx.h"
#include "DrawManager.hpp"
#include "InputManager.hpp"
#include "TextureManager.hpp"
#include "CollisionManager.hpp"
#include "Collider.h"
#include "AudioManager.hpp"
#include "StateManager.hpp"
#include "ServiceLocator.hpp"
#include "Engine.hpp"
#include "Info.h"

#include "GameState.h"

#include <iostream>

Engine::Engine()
{
	m_running = false;
	m_draw_manager = nullptr;
	m_input_manager = nullptr;
	m_texture_manager = nullptr;
	m_collision_manager = nullptr;
	m_audio_manager = nullptr;
	m_state_manager = nullptr;
	m_info = nullptr;
}

Engine::~Engine()
{
}

bool Engine::Initialize()
{
	m_window.create(sf::VideoMode(1920, 1080), "DevilTowerFight", sf::Style::Fullscreen);
	if (!m_window.isOpen())
		return false;

	// note(tommi) : disable key repeat spam
	// we only want to know when a key was pressed or released
	m_window.setKeyRepeatEnabled(false);

	// note(tommi): wait for vertical sync 60 Hz
	// read more at en.wikipedia.org/wiki/Analog_television#Vertical_synchronization
	m_window.setVerticalSyncEnabled(true);

	m_info = new Info;
	if (!m_info || !m_info->Initialize())
		return false;
	ServiceLocator<Info>::SetService(m_info);

	m_draw_manager = new DrawManager(&m_window);
	if (!m_draw_manager || !m_draw_manager->Initialize())
		return false;
	ServiceLocator<DrawManager>::SetService(m_draw_manager);

	m_input_manager = new InputManager;
	if (!m_input_manager || !m_input_manager->Initialize())
		return false;
	ServiceLocator<InputManager>::SetService(m_input_manager);

	m_texture_manager = new TextureManager;
	if (!m_texture_manager || !m_texture_manager->Initialize())
		return false;
	ServiceLocator<TextureManager>::SetService(m_texture_manager);

	m_audio_manager = new AudioManager;
	if (!m_audio_manager || !m_audio_manager->Initialize())
		return false;
	ServiceLocator<AudioManager>::SetService(m_audio_manager);

	m_state_manager = new StateManager;
	if (!m_state_manager || !m_state_manager->Initialize())
		return false;
	ServiceLocator<StateManager>::SetService(m_state_manager);

	
	return m_running = true;
}

void Engine::Shutdown()
{
	// note(tommi): here we delete the managers in the reverse order 
	// because of potential dependencies 
	if (m_state_manager)
	{
		m_state_manager->Shutdown();
		delete m_state_manager;
		m_state_manager = nullptr;
	}

	if (m_audio_manager)
	{
		m_audio_manager->Shutdown();
		delete m_audio_manager;
		m_audio_manager = nullptr;
	}

	if (m_texture_manager)
	{
		m_texture_manager->Shutdown();
		delete m_texture_manager;
		m_texture_manager = nullptr;
	}

	if (m_input_manager)
	{
		m_input_manager->Shutdown();
		delete m_input_manager;
		m_input_manager = nullptr;
	}

	if (m_draw_manager)
	{
		m_draw_manager->Shutdown();
		delete m_draw_manager;
		m_draw_manager = nullptr;
	}
}

void Engine::Run()
{		
	sf::Clock deltaClock;
	sf::Time current = deltaClock.getElapsedTime();

//	sf::Time deltaTime;
	while (m_running)
	{
		//deltaTime = deltaClock.restart();
		sf::Time now = deltaClock.getElapsedTime();
		sf::Time deltaTime = now - current;
		current = now;

		HandleOSEvents();

		m_window.clear();

		m_state_manager->Update(deltaTime.asSeconds());
			
		m_window.display();

		if (!m_window.isOpen())
			m_running = false;
	}
	m_window.close();
}

void Engine::TriggerShutDown()
{
	m_running = false;
}

// private
void Engine::HandleOSEvents()
	{
		// note(tommi): we want to take care of more events
		// but we only care about clearing the queue for now

		sf::Event event;
		m_input_manager->SetInputChar(' ');
		while (m_window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				m_running = false;
			}
			else if (event.type == sf::Event::KeyPressed)
			{
				m_input_manager->OnKeyboard(event.key.code, true);
			}
			else if (event.type == sf::Event::KeyReleased)
			{
				m_input_manager->OnKeyboard(event.key.code, false);
			}
			else if (event.type == sf::Event::TextEntered)
			{
				if (event.text.unicode < 128)
				{
					char letter = static_cast<char>(event.text.unicode);// event.key.code);
					m_input_manager->SetInputChar(letter);
				}
			}
			//goint to implement controllers if i can'
			if (event.type == sf::Event::JoystickConnected)
			{
				printf("joystick ");
				printf("%d", event.joystickConnect.joystickId + 1);
				printf(" is connected ");
			}
			if (event.type == sf::Event::JoystickDisconnected)
			{
				printf("joystick ");
				printf("%d", event.joystickConnect.joystickId + 1);
				printf(" has been disconnected ");
			}
			if (event.type == sf::Event::JoystickButtonPressed)
			{
				printf("joystick Button ");
				printf("%d", event.joystickButton.button);
				printf(" is pressed.");
			}
			/*if (event.type == sf::Event::JoystickMoved)
			{
				if (event.joystickMove.axis == sf::Joystick::X)
					printf("%d", event.joystickMove.position);
			}*/
		}
	}