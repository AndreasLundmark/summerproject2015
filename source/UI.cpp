#include "stdafx.h"
#include "ServiceLocator.hpp"
#include "TextureManager.hpp"
#include "DrawManager.hpp"
#include "Tower.h"

#include "Collider.h"
#include "UI.h"

UI::UI(int UItype, Tower* tower)
{
	TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();

	m_UItype = UItype;

	p_tower = tower;

	//Set visible
	m_visible = true;

	switch (m_UItype)
	{
	case 1:
		m_Sprite.setTexture(*texture_manager->LoadTexture("../assets/hpbarholder.png")); 
		m_Sprite.setPosition(30, 40);
		break;
	case 2:
		m_Sprite.setTexture(*texture_manager->LoadTexture("../assets/hpbarholder.png"));
		m_Sprite.setPosition(630, 40);
		break;
	case 3:
		m_Sprite.setTexture(*texture_manager->LoadTexture("../assets/bossbarholder.png"));
		m_Sprite.setPosition(1750, 10);
		break;
	case 4:
		m_Sprite.setTexture(*texture_manager->LoadTexture("../assets/instructions.png"));
		m_Sprite.setTextureRect(sf::IntRect(0, 246, 650, 85));
		m_Sprite.setPosition(1200, 800);
		m_visible = false;
		break;
	}

	p_collider = new Collider();
	p_collider->SetWidthHeight(m_Sprite.getTextureRect().width, m_Sprite.getTextureRect().height);
}

UI::~UI()
{
	if (p_collider)
	{
		delete p_collider;
		p_collider = nullptr;
	}
}

void UI::Update(float deltaTime)
{
		if (m_UItype == 4 && p_tower->GetBossDoorReadyBool() == true)
		m_visible = true;
}

sf::Sprite UI::GetSprite()
{
	return m_Sprite;
}

Collider* UI::GetCollider()
{
	return p_collider;
}

float UI::GetX()
{
	return m_Sprite.getPosition().x;
}

float UI::GetY()
{
	return m_Sprite.getPosition().y;
}

int UI::GetUIType()
{
	return m_UItype;
}

sf::Vector2f UI::GetPos()
{
	return m_Sprite.getPosition();
}

bool UI::IsVisible()
{
	return m_visible;
}

void UI::SetVisible(bool mode)
{
	m_visible = mode;
}

EEntityType UI::GetType()
{
	return ENTITY_UI;
}

void UI::SetPosition(float x, float y)
{
	m_Sprite.setPosition(x, y);
}
