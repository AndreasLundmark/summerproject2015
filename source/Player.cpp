//player.cpp
// the code for player 1 and 2

#include "stdafx.h"
#include "ServiceLocator.hpp"
#include "TextureManager.hpp"
#include "DrawManager.hpp"
#include "AudioManager.hpp"

#include "Player.h"
#include "Collider.h"
#include "AnimatedSprite.h"
#include "Animation.h"

Player::Player()
{
	TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();
	m_playerSprite.setTexture(*texture_manager->LoadTexture("../assets/idle7.png"));

	p_WalkCycleRight = new Animation("WalkRight");
	for (int i = 0; i < 5; i++)
	{
		p_WalkCycleRight->AddFrame(sf::IntRect(291 * i, 0, 291, 311));
	}
	p_WalkCycleRight->SetSpriteSheet(*texture_manager->LoadTexture("../assets/walk1.png"));

	p_WalkCycleLeft = new Animation("WalkLeft");
	for (int i = 0; i < 5; i++)
	{
		p_WalkCycleLeft->AddFrame(sf::IntRect(291 * i, 311, 291, 311));
	}
	p_WalkCycleLeft->SetSpriteSheet(*texture_manager->LoadTexture("../assets/walk1.png"));

	p_IdleLeft = new Animation("IdleLeft");
	for (int i = 0; i < 5; i++)
	{
		p_IdleLeft->AddFrame(sf::IntRect(291 * i, 311, 291, 311));
	}
	p_IdleLeft->SetSpriteSheet(*texture_manager->LoadTexture("../assets/idle7.png"));

	p_IdleRight = new Animation("IdleRight");
	for (int i = 0; i < 5; i++)
	{
		p_IdleRight->AddFrame(sf::IntRect(291 * i, 0, 291, 311));
	}
	p_IdleRight->SetSpriteSheet(*texture_manager->LoadTexture("../assets/idle7.png"));

	p_HitLeft = new Animation("HitLeft");
	for (int i = 0; i < 5; i++)
	{
		p_HitLeft->AddFrame(sf::IntRect(291 * i, 311, 291, 311));
	}
	p_HitLeft->SetSpriteSheet(*texture_manager->LoadTexture("../assets/phit1.png"));//taking damage

	p_HitRight = new Animation("HitRight");
	for (int i = 0; i < 5; i++)
	{
		p_HitRight->AddFrame(sf::IntRect(291 * i, 0, 291, 311));
	}
	p_HitRight->SetSpriteSheet(*texture_manager->LoadTexture("../assets/phit1.png"));//taking damage

	p_AttackLeft = new Animation("AttackLeft");
	for (int i = 0; i < 5; i++)
	{
		p_AttackLeft->AddFrame(sf::IntRect(291 * i, 301, 291, 301));
	}
	p_AttackLeft->SetSpriteSheet(*texture_manager->LoadTexture("../assets/attack1.png"));

	p_AttackRight = new Animation("AttackRight");
	for (int i = 0; i < 5; i++)
	{
		p_AttackRight->AddFrame(sf::IntRect(291 * i, 0, 291, 301));
	}
	p_AttackRight->SetSpriteSheet(*texture_manager->LoadTexture("../assets/attack1.png"));

	p_animatedSprite = new AnimatedSprite(&m_playerSprite, p_IdleRight, sf::seconds(0.2f), false, true);

	AudioManager* audio_manager = ServiceLocator<AudioManager>::GetService();

	/*footStepSound.setBuffer(*audio_manager->LoadSoundFromFile("../assets/....wav"));
	footStepSound.setBuffer(*audio_manager->LoadSoundFromFile("../assets/....wav"));
	lootSound.setBuffer(*audio_manager->LoadSoundFromFile("../assets/....wav"));*/

	//Starting point
	m_x = 100.0f;
	m_y = 680.0f;
	m_playerSprite.setPosition(m_x, m_y);

	//Set the players initial speed
	m_moveSpeed = 350.0f;

	m_soundTimer = 0.0f;

	//Initialize collider for the whole player sprite
	p_collider = new Collider();
	p_collider->SetWidthHeight(110, 110);
	p_collider->SetPosition(m_playerSprite.getPosition().x, m_playerSprite.getPosition().y);

	//Set visible
	m_visible = true;

	m_isInvincible = false;

	m_attacking1 = false; // attackerar spelaren?
	m_attacking2 = false; // attackerar spelaren?

	m_bossRules = false; // specialregler ifall bossen �r aktiv

	m_attackTimer = 0.0f;

	m_MaxHealth = 100;
	m_CurrentHealth = 100;

	m_MaxEnergy = 100;
	m_CurrentEnergy = 100;

	m_RespawnLives = 3;
	m_MaxRespawnLives = 6;

	// timers och r�knar hur m�nga g�nger spelaren har d�tt.
	m_deadTimer = 0.0f;
	m_deadTick = 0;
	
	m_invinsibleTimer = 0.0f;

	m_gold = 0;
	m_crystal = 0;

	m_state = STATE_NORMAL;
}

Player::~Player()
{
	if (p_collider)
	{
		delete p_collider;
		p_collider = nullptr;
	}

	//Removing all the animations:
	if (p_WalkCycleRight)
	{
		delete p_WalkCycleRight;
		p_WalkCycleRight = nullptr;
	}
	if (p_WalkCycleLeft)
	{
		delete p_WalkCycleLeft;
		p_WalkCycleLeft = nullptr;
	}
	if (p_HitLeft)
	{
		delete p_HitLeft;
		p_HitLeft = nullptr;
	}
	if (p_HitRight)
	{
		delete p_HitRight;
		p_HitRight = nullptr;
	}
	if (p_IdleRight)
	{
		delete p_IdleRight;
		p_IdleRight = nullptr;
	}
	if (p_IdleLeft)
	{
		delete p_IdleLeft;
		p_IdleLeft = nullptr;
	}
	if (p_animatedSprite)
	{
		delete p_animatedSprite;
		p_animatedSprite = nullptr;
	}
}

void Player::Update(float deltaTime)
{
	p_animatedSprite->Update(deltaTime);

	switch (m_state)
	{
	case STATE_NORMAL:
	{
		p_animatedSprite->setLooped(true);
		m_visible = true;
		break;
	}

	case STATE_DAMAGE:
	{
		p_animatedSprite->setLooped(false);
		p_animatedSprite->setFrameTime(sf::seconds(0.1f));

		if (p_animatedSprite->getAnimation()->GetAnimationName() == "WalkRight")
		{
			p_animatedSprite->setAnimation(p_HitRight);
		}
		else if (p_animatedSprite->getAnimation()->GetAnimationName() == "WalkLeft")
		{
			p_animatedSprite->setAnimation(p_HitLeft);
		}
		else if (p_animatedSprite->getAnimation()->GetAnimationName() == "IdleLeft")
		{
			p_animatedSprite->setAnimation(p_HitLeft);
		}
		else if (p_animatedSprite->getAnimation()->GetAnimationName() == "IdleRight")
		{
			p_animatedSprite->setAnimation(p_HitRight);
		}
		if (!p_animatedSprite->isPlaying())
		{
			m_state = STATE_NORMAL;

			if (p_animatedSprite->getAnimation() == p_AttackLeft)
			{
				p_animatedSprite->setAnimation(p_IdleLeft);
				p_animatedSprite->setLooped(true);//set animation looped again when we switch animation
				if (p_animatedSprite->getAnimation() == p_IdleLeft)// only move sprite if animation is...
				{
					p_animatedSprite->setFrameTime(sf::seconds(0.1));
				}

			}

			if (p_animatedSprite->getAnimation() == p_AttackRight)
			{
				p_animatedSprite->setAnimation(p_IdleRight);
				p_animatedSprite->setLooped(true);//set animation looped again when we switch animation
				if (p_animatedSprite->getAnimation() == p_IdleRight)// only move sprite if animation is...
				{
					p_animatedSprite->setFrameTime(sf::seconds(0.1));
				}
			}
		}
		break;
	}

	case STATE_PLAYERDEAD:
	{
		if (m_deadTimer > 0)
		{
			m_deadTimer -= 1.0f;
		}
		if (m_deadTimer <= 0 && m_RespawnLives >= 1)
			Respawn();
		else
		{

		}
		break;
	}

	case STATE_ATTACK:
	{
		if (m_CurrentEnergy >= 1)
			m_CurrentEnergy -= 1;
		p_animatedSprite->setFrameTime(sf::seconds(0.15));
		p_animatedSprite->setLooped(false);

		if (m_attackDirection == true && m_CurrentEnergy >= 10) // hit right
		{
			p_animatedSprite->setAnimation(p_AttackRight);
		}

		if (m_attackDirection == false && m_CurrentEnergy >= 10) // hit left
			p_animatedSprite->setAnimation(p_AttackLeft);

		if (!p_animatedSprite->isPlaying())
		{
			m_state = STATE_NORMAL;

			if (p_animatedSprite->getAnimation() == p_AttackLeft)
			{
				p_animatedSprite->setAnimation(p_IdleLeft);
				p_animatedSprite->setLooped(true);//set animation looped again when we switch animation
				if (p_animatedSprite->getAnimation() == p_IdleLeft)// only move sprite if animation is...
				{
					p_animatedSprite->setFrameTime(sf::seconds(0.1));
				}
			}

			if (p_animatedSprite->getAnimation() == p_AttackRight)
			{
				p_animatedSprite->setAnimation(p_IdleRight);
				p_animatedSprite->setLooped(true);//set animation looped again when we switch animation
				if (p_animatedSprite->getAnimation() == p_IdleRight)// only move sprite if animation is...
				{
					p_animatedSprite->setFrameTime(sf::seconds(0.1));
				}
			}
		}
		break;
	}
	}

	m_x = m_playerSprite.getPosition().x;
	m_y = m_playerSprite.getPosition().y;

	if (m_invinsibleTimer >= 0)
	{
		m_isInvincible = true;
		m_invinsibleTimer -= 1;
	}
	else if (m_invinsibleTimer<=0)
		m_isInvincible = false;

	//Update the player's whole body collider using the player's current position
	if (m_attackDirection == true)//right
	{
		if (m_attacking1==true)
			p_collider->Refresh(m_x, m_y);
		else
		p_collider->Refresh(m_x + 70, m_y + 80);
	}
	if (m_attackDirection == false)//left
	{
		if (m_attacking1==true)
		p_collider->Refresh(m_x, m_y);
		else
		p_collider->Refresh(m_x + 100, m_y + 80);
	}

	if (m_attacking1 == true)
	{
		p_collider->SetWidthHeight(291, 311);
	}
	else
	{
		p_collider->SetWidthHeight(110, 110);
	}

	if (m_attackTimer >= 1)
	{
		m_attackTimer -= 1;
	}
	if (m_attackTimer <= 0)
		m_attacking1 = false;
	//�ka energin �ver tid
	if (m_CurrentEnergy < m_MaxEnergy)
		m_CurrentEnergy += 0.09;
	//D�da spelaren om han har mindre �n 1 i m_currenthealth
	if (m_CurrentHealth < 1 && m_visible == true)
	{
		KillPlayer();
	}
	//f�rhindra att spelaren f�r �ver max liv och energi
	if (m_CurrentHealth > m_MaxHealth)
		m_CurrentHealth = m_MaxHealth;
	if (m_CurrentEnergy> m_MaxEnergy)
		m_CurrentEnergy = m_MaxEnergy;
}

void Player::SetFootStep(std::string fileName)
{
	AudioManager* audio_manager = ServiceLocator<AudioManager>::GetService();
	if (footStepSound.getBuffer() != audio_manager->LoadSoundFromFile("../assets/" + fileName))
	{
		footStepSound.stop();
		footStepSound.setBuffer(*audio_manager->LoadSoundFromFile("../assets/" + fileName));
	}
}

void Player::PlayFootStep()
{
	//Checking if the status of the soundsource is playing or not.
	if (footStepSound.getStatus() != sf::SoundSource::Status::Playing)
	{
		footStepSound.play();
	}
}

sf::Sprite Player::GetSprite()
{
	return m_playerSprite;
}

Collider* Player::GetCollider()
{
	return p_collider;
}

float Player::GetX()
{
	return m_playerSprite.getPosition().x;
}

float Player::GetY()
{
	return m_playerSprite.getPosition().y;
}

int Player::GetMaxHealth()
{
	return m_MaxHealth;
}

int Player::GetCurrentHealth()
{
	return m_CurrentHealth;
}

int Player::GetMaxEnergy()
{
	return m_MaxEnergy;
}

int Player::GetCurrentEnergy()
{
	return m_CurrentEnergy;
}

int Player::GetRespawnLives()
{
	return m_RespawnLives;
}

int Player::GetMaxRespawnLives()
{
	return m_MaxRespawnLives;
}

void Player::IncreaseRespawnLives(int lives)
{
	if (m_RespawnLives<m_MaxRespawnLives)
		m_RespawnLives += lives;
}

sf::Vector2f Player::GetPos()
{
	return m_playerSprite.getPosition();
}

sf::Vector2f Player::GetSpriteCenter()
{
	return sf::Vector2f(GetPos().x + m_playerSprite.getTextureRect().width / 2,
		GetPos().y + m_playerSprite.getTextureRect().height / 2);
}

void Player::Move(MoveDirection dir, float deltatime)
{
	TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();

	if (dir == DIR_RIGHT && m_x<1300 && m_state != STATE_ATTACK) // om d�rren till bossen �r st�ngd
	{
		m_attackDirection = true; // what dir the player should attack
		m_moveDirection.x = 1.0f;
	}

	else if (dir == DIR_RIGHT && m_x<1679 && m_state != STATE_ATTACK && m_bossRules==true) // om d�rren till bossen �r st�ngd
	{
		m_attackDirection = true; // what dir the player should attack
		m_moveDirection.x = 1.0f;
	}

	if (dir == DIR_LEFT && m_x>90 && m_state != STATE_ATTACK)
	{
		m_attackDirection = false;
		m_moveDirection.x = -1.0f;
	}

	if (dir == DIR_LEFT && m_x>0 && m_state != STATE_ATTACK && m_bossRules == true)
	{
		m_attackDirection = false;
		m_moveDirection.x = -1.0f;
	}

	if (dir == DIR_UP && m_y>300 && m_state != STATE_ATTACK)
	{
		m_moveDirection.y = -1.0f;
	}

	else if (dir == DIR_DOWN && m_y<680 && m_state != STATE_ATTACK)
	{
		m_moveDirection.y = 1.0f;
	}

	if (dir == DIR_IDLE)
	{
		m_moveDirection = sf::Vector2f(0.0f, 0.0f);
	}
	else{
		PlayFootStep();
		p_animatedSprite->setFrameTime(sf::seconds(0.4));
	}


	if (m_moveDirection.x == -1)
	{
		if (m_state == STATE_NORMAL)
		{
			p_animatedSprite->setAnimation(p_WalkCycleLeft);
		}

	}
	else if (m_moveDirection.x == 1)
	{
		if (m_state == STATE_NORMAL)
		{
			p_animatedSprite->setAnimation(p_WalkCycleRight);
		}
	}
	else if (m_moveDirection.y != 0)
	{
		if (m_state == STATE_NORMAL)
		{
			if (p_animatedSprite->getAnimation() == p_IdleLeft)
				p_animatedSprite->setAnimation(p_WalkCycleLeft);

			if (p_animatedSprite->getAnimation() == p_IdleRight)
				p_animatedSprite->setAnimation(p_WalkCycleRight);
		}
	}
	else if (m_moveDirection == sf::Vector2f(0.0f, 0.0f))
	{
		if (m_state == STATE_NORMAL)
		{
			p_animatedSprite->setFrameTime(sf::seconds(0.4));

			if (p_animatedSprite->getAnimation() == p_WalkCycleLeft)
				p_animatedSprite->setAnimation(p_IdleLeft);

			if (p_animatedSprite->getAnimation() == p_WalkCycleRight && m_x<1290) // if the player
				p_animatedSprite->setAnimation(p_IdleRight);

			if (p_animatedSprite->getAnimation() == p_WalkCycleRight && m_x>1290 && m_bossRules==false)
				p_animatedSprite->setAnimation(p_WalkCycleRight);

			else if (p_animatedSprite->getAnimation() == p_WalkCycleRight && m_x>1290 && m_bossRules == true)
				p_animatedSprite->setAnimation(p_IdleRight);
		}
	}

	m_playerSprite.move(m_moveSpeed * deltatime * m_moveDirection.x, m_moveSpeed * deltatime * m_moveDirection.y);
	m_moveDirection = sf::Vector2f(0.0f, 0.0f);
}

void Player::SetWalkMode(WalkMode mode)
{
	m_walkMode = mode;
}

void Player::KillPlayer()
{
	m_deadTimer = 10.0f;
	m_visible = false;
	m_state = STATE_PLAYERDEAD;
}

void Player::Respawn() // respawn position
{
	//Starting point
	m_x = 100;
	m_y = 680;
	m_playerSprite.setPosition(m_x, m_y);
	p_animatedSprite->setAnimation(p_IdleRight);

	//Set the players initial speed
	m_moveSpeed = 350.0f;

	//Set visible
	m_visible = true;

	m_CurrentHealth = m_MaxHealth * 0.6;
	m_CurrentEnergy = m_MaxEnergy * 0.6;

	m_attackTimer = 0.0f;

	m_invinsibleTimer = 100.0f;

	m_RespawnLives -= 1;

	m_state = STATE_NORMAL;
}

bool Player::IsVisible()
{
	return m_visible;
}

EEntityType Player::GetType()
{
	return ENTITY_PLAYER;
}

void Player::SetPosition(float x, float y)
{
	m_playerSprite.setPosition(x, y);
}

PlayerState Player::GetState()
{
	return m_state;
}

int Player::GetGold()
{
	return m_gold;
}

void Player::SetState(PlayerState state)
{
	m_state = state;
}

bool Player::IsInvincible()
{
	return m_isInvincible;
}

void Player::SetAlive(bool mode)
{
	m_visible = mode;
}

AnimatedSprite* Player::GetAnimatedSprite()
{
	return p_animatedSprite;
}

void Player::ReduceHealth(int damage)
{
	if (m_CurrentHealth > 0 && m_isInvincible == false)
	{
		m_state = STATE_DAMAGE;
		m_CurrentHealth -= damage;
	}
}

void Player::IncreaseHealth(int Health)
{
	if (m_CurrentHealth<m_MaxHealth)
		m_CurrentHealth += Health;
}
void Player::ReduceEnergy(int usage)
{
	if (m_CurrentEnergy>0)
		m_CurrentEnergy -= usage;
}

void Player::IncreaseEnergy(int energy)
{
	if (m_CurrentEnergy<m_MaxEnergy)
		m_CurrentEnergy += energy;
}

void Player::IncreaseGold(int gold)
{
	m_gold += gold;
}

void Player::IncreaseCrystal(int crystal)
{
	m_crystal += crystal;
}

bool Player::GetActiveAttack()
{
	return m_attacking1;
}

void Player::ActivateAttack()
{
	if (m_attackTimer <= 0)
	{
		m_attacking1 = true;
		m_attackTimer = 60;
	}
}

void Player::ActivateBossRules()
{
	m_bossRules = true;
}
