// TextureManager.cpp

#include "stdafx.h"
#include "TextureManager.hpp"

TextureManager::TextureManager()
{
}

TextureManager::~TextureManager()
{
}

bool TextureManager::Initialize()
{
	return true;
}

void TextureManager::Shutdown()
{
	auto itr = m_textures.begin();
	while (itr != m_textures.end())
	{
		delete itr->second;
		++itr;
	}
	m_textures.clear();
}

sf::Texture* TextureManager::LoadTexture(std::string filePath)
{
	auto itr = m_textures.find(filePath);
	if (itr == m_textures.end())
	{
		sf::Texture* texture = new sf::Texture;
		texture->loadFromFile(filePath);

		m_textures.insert(std::pair<std::string, sf::Texture*>(filePath, texture));
		itr = m_textures.find(filePath);
	}

	return itr->second;
}