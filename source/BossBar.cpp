#include "stdafx.h"
#include "ServiceLocator.hpp"
#include "TextureManager.hpp"
#include "DrawManager.hpp"
#include "AudioManager.hpp"
#include "Player.h"
#include "Tower.h"

#include "Collider.h"
#include "BossBar.h"
#include "AnimatedSprite.h"
#include "Animation.h"

BossBar::BossBar(Player* player,Tower* tower, int bartype)
{
	p_player = player;
	p_tower = tower;
	m_bartype = bartype;

	TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();

	//Set sprite
	switch (m_bartype) // har kvar bartype ifall jag kommer beh�va koden till andra m�tare.
	{
	case 1:
		m_BossBarSprite.setTexture(*texture_manager->LoadTexture("../assets/progressbar.png"));
		m_BossBarSprite.setPosition(1790, 610);
		break;
	case 2:
		m_BossBarSprite.setTexture(*texture_manager->LoadTexture("../assets/progressbar.png"));
		m_BossBarSprite.setPosition(1790, 610);
		break;
	}

	m_BossBarSprite.setTextureRect(sf::IntRect(0, 0, 600, 160));

	p_BossBarAnimation = new Animation("BossBarAnimation");
	p_BossBarAnimation->AddFrame(sf::IntRect(0, 0, 400, 40));
	p_BossBarAnimation->AddFrame(sf::IntRect(0, 40, 400, 40));
	p_BossBarAnimation->AddFrame(sf::IntRect(0, 80, 400, 40));
	p_BossBarAnimation->SetSpriteSheet(*texture_manager->LoadTexture("../assets/progressbar.png"));

	p_animatedSprite = new AnimatedSprite(&m_BossBarSprite, p_BossBarAnimation, sf::seconds(0.2f), false, true);

	p_collider = new Collider();
	p_collider->SetWidthHeight(m_BossBarSprite.getTextureRect().width, m_BossBarSprite.getTextureRect().height);

	//Set visible
	m_visible = true;
	m_value = 0;
}

BossBar::~BossBar()
{
	if (p_collider)
	{
		delete p_collider;
		p_collider = nullptr;
	}
}

void BossBar::Update(float deltaTime)
{
	m_x = m_BossBarSprite.getPosition().x;
	m_y = m_BossBarSprite.getPosition().y;

	m_BossBarSprite.setPosition(1790, 610 - m_value);

	p_animatedSprite->Update(deltaTime);

	p_animatedSprite->setLooped(true);
	p_animatedSprite->setFrameTime(sf::seconds(0.2f));
	p_animatedSprite->setAnimation(p_BossBarAnimation);

	p_animatedSprite->play();

	if (m_bartype == 1) // BossMeter // boss
		m_value = p_tower->GetBossClimb();
	
	if (m_bartype == 2) // BossMeter // player
		m_value = p_tower->GetPlayerClimb();

	if (m_bartype == 1)
		m_BossBarSprite.setTextureRect(sf::IntRect(0, 0, 40, m_value));
	if (m_bartype == 2)
		m_BossBarSprite.setTextureRect(sf::IntRect(40, 0, 40, m_value));
}

sf::Sprite BossBar::GetSprite()
{
	return m_BossBarSprite;
}

Collider* BossBar::GetCollider()
{
	return p_collider;
}

float BossBar::GetX()
{
	return m_BossBarSprite.getPosition().x;
}

float BossBar::GetY()
{
	return m_BossBarSprite.getPosition().y;
}

int BossBar::GetBarType()
{
	return m_bartype;
}

sf::Vector2f BossBar::GetPos()
{
	return m_BossBarSprite.getPosition();
}

bool BossBar::IsVisible()
{
	return m_visible;
}

void BossBar::SetVisible(bool mode)
{
	m_visible = mode;
}

EEntityType BossBar::GetType()
{
	return ENTITY_BOSSBAR;
}

void BossBar::SetPosition(float x, float y)
{
	m_BossBarSprite.setPosition(x, y);
}
