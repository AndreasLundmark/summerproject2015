#include "stdafx.h"
#include "ServiceLocator.hpp"
#include "TextureManager.hpp"
#include "DrawManager.hpp"
#include "AudioManager.hpp"

#include "Collider.h"
#include "MenuButton.h"

MenuButton::MenuButton(int buttonType, int x, int y)
{
	m_buttonType = (ButtonType)buttonType;

	TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();

	//Set sprite
	m_MenuButtonSprite.setTexture(*texture_manager->LoadTexture("../assets/Buttons.png"));
	m_MenuButtonSprite.setTextureRect(sf::IntRect(0, 0, 500, 250));
	m_MenuButtonSprite.setPosition(x, y);

	p_collider = new Collider();
	p_collider->SetWidthHeight(m_MenuButtonSprite.getTextureRect().width-60, m_MenuButtonSprite.getTextureRect().height-60);
	p_collider->SetPosition(m_MenuButtonSprite.getPosition().x, m_MenuButtonSprite.getPosition().y);

	//Set visible
	m_visible = true;
	//hover object
	m_hover = false;
	//knapp tryckning
	m_buttonpress = false;
}


MenuButton::~MenuButton()
{
	if (p_collider)
	{
		delete p_collider;
		p_collider = nullptr;
	}
}

void MenuButton::Update(float deltaTime)
{
	m_x = m_MenuButtonSprite.getPosition().x;
	m_y = m_MenuButtonSprite.getPosition().y;

	p_collider->SetPosition(m_MenuButtonSprite.getPosition().x+30, m_MenuButtonSprite.getPosition().y+30);

	if (m_hover == false)
	{
		m_MenuButtonSprite.setTextureRect(sf::IntRect(0, m_buttonType * 250, 500, 250));
	}
	if (m_hover == true)
	{
		m_MenuButtonSprite.setTextureRect(sf::IntRect(500, m_buttonType * 250, 500, 250));
	}
}

sf::Sprite MenuButton::GetSprite()
{
	return m_MenuButtonSprite;
}

Collider* MenuButton::GetCollider()
{
	return p_collider;
}

float MenuButton::GetX()
{
	return m_MenuButtonSprite.getPosition().x;
}

float MenuButton::GetY()
{
	return m_MenuButtonSprite.getPosition().y;
}

sf::Vector2f MenuButton::GetPos()
{
	return m_MenuButtonSprite.getPosition();
}

bool MenuButton::IsVisible()
{
	return m_visible;
}

void MenuButton::SetVisible(bool mode)
{
	m_visible = mode;
}

void MenuButton::Hover(bool mode)
{
	m_hover = mode;
}

bool MenuButton::GetHover()
{
	return m_hover;
}

EEntityType MenuButton::GetType()
{
	return ENTITY_MENUBUTTON;
}

ButtonType  MenuButton::GetButtonType()
{
	return m_buttonType;
}

void MenuButton::SetPosition(float x, float y)
{
	m_MenuButtonSprite.setPosition(x, y);
}