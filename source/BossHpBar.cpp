#include "stdafx.h"
#include "ServiceLocator.hpp"
#include "TextureManager.hpp"
#include "DrawManager.hpp"
#include "AudioManager.hpp"
#include "Boss.h"

#include "Collider.h"
#include "BossHpBar.h"
#include "AnimatedSprite.h"
#include "Animation.h"

BossHpBar::BossHpBar(Boss* boss)
{
	p_boss = boss;

	TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();

	//Set sprite
	
	m_HpBarSprite.setTexture(*texture_manager->LoadTexture("../assets/hpbar.png"));
	m_HpBarSprite.setPosition(700, 1000);
	m_HpBarSprite.setTextureRect(sf::IntRect(0, 0, 600, 160));

	p_HpBarAnimation = new Animation("HpBarAnimation");
	p_HpBarAnimation->AddFrame(sf::IntRect(0, 0, 400, 40));
	p_HpBarAnimation->AddFrame(sf::IntRect(0, 40, 400, 40));
	p_HpBarAnimation->AddFrame(sf::IntRect(0, 80, 400, 40));
	p_HpBarAnimation->SetSpriteSheet(*texture_manager->LoadTexture("../assets/hpbar.png"));

	p_animatedSprite = new AnimatedSprite(&m_HpBarSprite, p_HpBarAnimation, sf::seconds(0.2f), false, true);

	p_collider = new Collider();
	p_collider->SetWidthHeight(m_HpBarSprite.getTextureRect().width, m_HpBarSprite.getTextureRect().height);

	//Set visible
	m_visible = true;
	//PlayerHealth
	m_value = 0;
}

BossHpBar::~BossHpBar()
{
	if (p_collider)
	{
		delete p_collider;
		p_collider = nullptr;
	}
}

void BossHpBar::Update(float deltaTime)
{
	m_x = m_HpBarSprite.getPosition().x;
	m_y = m_HpBarSprite.getPosition().y;

	p_animatedSprite->Update(deltaTime);

	p_animatedSprite->setLooped(true);
	p_animatedSprite->setFrameTime(sf::seconds(0.2f));
	p_animatedSprite->setAnimation(p_HpBarAnimation);

	p_animatedSprite->play();
	m_value = p_boss->GetCurrentHealth() * 400 / p_boss->GetMaxHealth(); 
	m_HpBarSprite.setTextureRect(sf::IntRect(0, 0, m_value, 40));
}

sf::Sprite BossHpBar::GetSprite()
{
	return m_HpBarSprite;
}

Collider* BossHpBar::GetCollider()
{
	return p_collider;
}

float BossHpBar::GetX()
{
	return m_HpBarSprite.getPosition().x;
}

float BossHpBar::GetY()
{
	return m_HpBarSprite.getPosition().y;
}

sf::Vector2f BossHpBar::GetPos()
{
	return m_HpBarSprite.getPosition();
}

bool BossHpBar::IsVisible()
{
	return m_visible;
}

void BossHpBar::SetVisible(bool mode)
{
	m_visible = mode;
}

EEntityType BossHpBar::GetType()
{
	return ENTITY_HPBAR;
}

void BossHpBar::SetPosition(float x, float y)
{
	m_HpBarSprite.setPosition(x, y);
}
