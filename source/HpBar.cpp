#include "stdafx.h"
#include "ServiceLocator.hpp"
#include "TextureManager.hpp"
#include "DrawManager.hpp"
#include "AudioManager.hpp"
#include "Player.h"

#include "Collider.h"
#include "HpBar.h"
#include "AnimatedSprite.h"
#include "Animation.h"

HpBar::HpBar(Player* player, int bartype)
{
	p_player = player;
	m_bartype = bartype;

	TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();

	//Set sprite
	switch (m_bartype)
	{
	case 1:
		m_HpBarSprite.setTexture(*texture_manager->LoadTexture("../assets/hpbar.png"));
		m_HpBarSprite.setPosition(100, 70);
		break;
	case 2:
		m_HpBarSprite.setTexture(*texture_manager->LoadTexture("../assets/actionbar.png"));
		m_HpBarSprite.setPosition(100, 110);
		break;
	case 3:
		m_HpBarSprite.setTexture(*texture_manager->LoadTexture("../assets/actionbar.png"));
		m_HpBarSprite.setPosition(100, 160);
		break;
	case 4:
		m_HpBarSprite.setTexture(*texture_manager->LoadTexture("../assets/hpbar.png"));
		m_HpBarSprite.setPosition(700, 70);
		break;
	case 5:
		m_HpBarSprite.setTexture(*texture_manager->LoadTexture("../assets/actionbar.png"));
		m_HpBarSprite.setPosition(700, 110);
		break;
	case 6:
		m_HpBarSprite.setTexture(*texture_manager->LoadTexture("../assets/actionbar.png"));
		m_HpBarSprite.setPosition(700, 160);
		break;
	}
	m_HpBarSprite.setTextureRect(sf::IntRect(0, 0, 600, 160));
	

	p_HpBarAnimation = new Animation("HpBarAnimation");
	for (int i = 0; i < 4; i++)
	{
		p_HpBarAnimation->AddFrame(sf::IntRect(0, 160 * i, 600, 160));
	}
	switch (bartype)
	{
	case 1:
		p_HpBarAnimation->SetSpriteSheet(*texture_manager->LoadTexture("../assets/hpbar.png"));
		break;
	case 2:
		p_HpBarAnimation->SetSpriteSheet(*texture_manager->LoadTexture("../assets/actionbar.png"));
		break;
	case 3:
		p_HpBarAnimation->SetSpriteSheet(*texture_manager->LoadTexture("../assets/lives.png"));
		break;
	case 4:
		p_HpBarAnimation->SetSpriteSheet(*texture_manager->LoadTexture("../assets/hpbar.png"));
		break;
	case 5:
		p_HpBarAnimation->SetSpriteSheet(*texture_manager->LoadTexture("../assets/actionbar.png"));
		break;
	case 6:
		p_HpBarAnimation->SetSpriteSheet(*texture_manager->LoadTexture("../assets/lives.png"));
		break;
	}
	p_animatedSprite = new AnimatedSprite(&m_HpBarSprite, p_HpBarAnimation, sf::seconds(0.2f), false, true);

	p_collider = new Collider();
	p_collider->SetWidthHeight(0, 0);

	//Set visible
	m_visible = true;
	//PlayerHealth
	m_value = 0;
}

HpBar::~HpBar()
{
	if (p_collider)
	{
		delete p_collider;
		p_collider = nullptr;
	}
}

void HpBar::Update(float deltaTime)
{
	m_x = m_HpBarSprite.getPosition().x;
	m_y = m_HpBarSprite.getPosition().y;

	p_animatedSprite->Update(deltaTime);

	p_animatedSprite->setLooped(true);
	p_animatedSprite->setFrameTime(sf::seconds(0.2f));
	p_animatedSprite->setAnimation(p_HpBarAnimation);

	p_animatedSprite->play();

	if (m_bartype == 1) // Health
	m_value = p_player->GetCurrentHealth() * 400 / p_player->GetMaxHealth();
	if (m_bartype == 2) // Energy
	m_value = p_player->GetCurrentEnergy() * 400 / p_player->GetMaxEnergy();
	if (m_bartype == 3) // Lives
		m_value = p_player->GetRespawnLives() * 400 / p_player->GetMaxRespawnLives();

	if (m_bartype == 4) // Health
		m_value = p_player->GetCurrentHealth() * 400 / p_player->GetMaxHealth();
	if (m_bartype == 5) // Energy
		m_value = p_player->GetCurrentEnergy() * 400 / p_player->GetMaxEnergy();
	if (m_bartype == 6) // Lives
		m_value = p_player->GetRespawnLives() * 400 / p_player->GetMaxRespawnLives();

	if (m_bartype == 1 || 2 || 4 || 5)
	m_HpBarSprite.setTextureRect(sf::IntRect(0, 0, m_value, 40));
}

sf::Sprite HpBar::GetSprite()
{
	return m_HpBarSprite;
}

Collider* HpBar::GetCollider()
{
	return p_collider;
}

float HpBar::GetX()
{
	return m_HpBarSprite.getPosition().x;
}

float HpBar::GetY()
{
	return m_HpBarSprite.getPosition().y;
}

sf::Vector2f HpBar::GetPos()
{
	return m_HpBarSprite.getPosition();
}

bool HpBar::IsVisible()
{
	return m_visible;
}

void HpBar::SetVisible(bool mode)
{
	m_visible = mode;
}

EEntityType HpBar::GetType()
{
	return ENTITY_HPBAR;
}

void HpBar::SetPosition(float x, float y)
{
	m_HpBarSprite.setPosition(x, y);
}
