//GameState.cpp

#include "stdafx.h"
#include "ServiceLocator.hpp"
#include "TextureManager.hpp"
#include "DrawManager.hpp"
#include "Collider.h"
#include "CollisionManager.hpp"
#include "AudioManager.hpp"
#include "InputManager.hpp"

#include <iostream>

#include "AbstractState.hpp"
#include "GameState.h"

#include "Randomizer.h"

#include "Player.h"
#include "Attack.h"
#include "Enemy.h"
#include "EnemySpawner.h"
#include "Boss.h"
#include "BossHpBar.h"
#include "Tower.h"
#include "Loot.h"
#include "HpBar.h"
#include "BossBar.h"
#include "UI.h"
#include "Info.h"

GameState::GameState()
{
	m_state_bool = true; //F�r Return true, skriv return m_state_bool
	m_win_bool = false;

	DrawManager* draw_manager = ServiceLocator<DrawManager>::GetService();
	TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();
	AudioManager* audio_manager = ServiceLocator<AudioManager>::GetService();

	//bakgrundsmusik:
	music = audio_manager->LoadMusicFromFile("../assets/sounds/battle1.wav");
	music->setLoop(true);
	music->setVolume(30);

	//Kameran:
	view1 = new sf::View(sf::FloatRect(0.0f, 0.0f, 1920.0f, 1080.0f));

	if (!font.loadFromFile("../assets/OpenSans-Bold.ttf"))
	{
		printf("can't find font");
	}
	// Create a text
	text.setFont(font);
	text.setCharacterSize(35);
	text.setStyle(sf::Text::Bold);
	text.setColor(sf::Color(0, 0, 0, 255));
}

GameState::~GameState()
{
	//Delete the view pointer
	if (view1)
	{
		delete view1;
		view1 = nullptr;
	}

	//Delete all the entities (alla objekt)
		auto it = m_entities.begin();
		while (it != m_entities.end())
		{
			if (*it)
				delete (*it);
			++it;
		}
		m_entities.clear();
	
}

bool GameState::Enter()
{
	// note(tommi): register to listen for all input actions
	InputManager* input_manager = ServiceLocator<InputManager>::GetService();
	input_manager->RegisterKeyActionListener(Action::LEFT, this, &GameState::OnAction);
	input_manager->RegisterKeyActionListener(Action::RIGHT, this, &GameState::OnAction);
	input_manager->RegisterKeyActionListener(Action::UP, this, &GameState::OnAction);
	input_manager->RegisterKeyActionListener(Action::DOWN, this, &GameState::OnAction);
	input_manager->RegisterKeyActionListener(Action::ATTACK1, this, &GameState::OnAction);
	input_manager->RegisterKeyActionListener(Action::MENU, this, &GameState::OnAction);
	input_manager->RegisterKeyActionListener(Action::SELECT3, this, &GameState::OnAction);
	input_manager->RegisterKeyActionListener(Action::SELECT4, this, &GameState::OnAction);
	input_manager->RegisterKeyActionListener(Action::LEFT2, this, &GameState::OnAction);
	input_manager->RegisterKeyActionListener(Action::RIGHT2, this, &GameState::OnAction);
	input_manager->RegisterKeyActionListener(Action::UP2, this, &GameState::OnAction);
	input_manager->RegisterKeyActionListener(Action::DOWN2, this, &GameState::OnAction);
	input_manager->RegisterKeyActionListener(Action::ATTACK12, this, &GameState::OnAction);
	input_manager->RegisterKeyActionListener(Action::MENU2, this, &GameState::OnAction);
	input_manager->RegisterKeyActionListener(Action::SELECT32, this, &GameState::OnAction);
	input_manager->RegisterKeyActionListener(Action::SELECT42, this, &GameState::OnAction);
	music->play();
	DrawManager* draw_manager = ServiceLocator<DrawManager>::GetService();

	draw_manager->GetRenderWindow()->setMouseCursorVisible(false); // g�r musen synlig / osynlig

	BeginningSpawnObjects(); // alla objekt som ska skapas i b�rjan av spelomg�ngen.

	return true;
}

void GameState::Exit()
{
	// note(tommi): unregister all actions listeners
	InputManager* input_manager = ServiceLocator<InputManager>::GetService();
	input_manager->UnregisterKeyActionListener(Action::LEFT, this);
	input_manager->UnregisterKeyActionListener(Action::RIGHT, this);
	input_manager->UnregisterKeyActionListener(Action::UP, this);
	input_manager->UnregisterKeyActionListener(Action::DOWN, this);
	input_manager->UnregisterKeyActionListener(Action::ATTACK1, this);
	input_manager->UnregisterKeyActionListener(Action::MENU, this);
	input_manager->UnregisterKeyActionListener(Action::SELECT3, this);
	input_manager->UnregisterKeyActionListener(Action::SELECT4, this);
	input_manager->UnregisterKeyActionListener(Action::LEFT2, this);
	input_manager->UnregisterKeyActionListener(Action::RIGHT2, this);
	input_manager->UnregisterKeyActionListener(Action::UP2, this);
	input_manager->UnregisterKeyActionListener(Action::DOWN2, this);
	input_manager->UnregisterKeyActionListener(Action::ATTACK12, this);
	input_manager->UnregisterKeyActionListener(Action::MENU2, this);
	input_manager->UnregisterKeyActionListener(Action::SELECT32, this);
	input_manager->UnregisterKeyActionListener(Action::SELECT42, this);
	music->stop();

	//n�r vi l�mnar niv�n blir musen synlig vid menyn igen
	DrawManager* draw_manager = ServiceLocator<DrawManager>::GetService();
	draw_manager->GetRenderWindow()->setMouseCursorVisible(true);

	if (p_player && p_player2)
		p_info->GoldSaveToFile(p_player->GetGold() + p_player2->GetGold());
	else if (!p_player2 && p_player)
		p_info->GoldSaveToFile(p_player->GetGold());

	//Delete all the entities (alla objekt)
		auto it = m_entities.begin();
		while (it != m_entities.end())
		{
			if (*it)
				delete (*it);
			++it;
		}
		m_entities.clear();
}

void GameState::BeginningSpawnObjects() // alla objekt som ska skapas i b�rjan av spelomg�ngen. Bara f�r att vara l�ttare att l�sa och separera fr�n ovanst�ende kod.
{
	//f� v�rderna fr�n gso filen
	getplayers = p_info->ReadFile(1,"../conf/gso.txt");
	gettower = p_info->ReadFile(2, "../conf/gso.txt");
	getshopitem = p_info->ReadFile(3, "../conf/gso.txt");

	//skapa objekten
	p_tower = new Tower(p_player, p_bossbar);
	m_entities.push_back(p_tower);

	if (getshopitem == 2)
		p_tower->SetPlayerClimb(250);

	// boss attacks
	p_attack = new Attack(0);
	m_entities.push_back(p_attack);
	p_attack2 = new Attack(221);
	m_entities.push_back(p_attack2);
	p_attack3 = new Attack(442);
	m_entities.push_back(p_attack3);
	p_attack4 = new Attack(663);
	m_entities.push_back(p_attack4);
	p_attack5 = new Attack(884);
	m_entities.push_back(p_attack5);
	p_attack6 = new Attack(1105);
	m_entities.push_back(p_attack6);

	p_player = new Player();
	m_entities.push_back(p_player);

	if (getplayers == 2)
	{
		p_player2 = new Player();
		m_entities.push_back(p_player2);
	}

	p_hpbar = new HpBar(p_player, 1);
	m_entities.push_back(p_hpbar);
	p_hpbar = new HpBar(p_player, 2);
	m_entities.push_back(p_hpbar);
	p_hpbar = new HpBar(p_player, 3);
	m_entities.push_back(p_hpbar);

	if (p_player2)
	{
		p_hpbar = new HpBar(p_player2, 4);
		m_entities.push_back(p_hpbar);
		p_hpbar = new HpBar(p_player2, 5);
		m_entities.push_back(p_hpbar);
		p_hpbar = new HpBar(p_player2, 6);
		m_entities.push_back(p_hpbar);
	}

	p_bossbar = new BossBar(p_player, p_tower, 1);
	m_entities.push_back(p_bossbar);
	p_bossbar = new BossBar(p_player, p_tower, 2);
	m_entities.push_back(p_bossbar);

	p_ui = new UI(1, p_tower); //hpbarholderp1
	m_entities.push_back(p_ui);
	if (p_player2)
	{
		p_ui = new UI(2, p_tower); //hpbarholderp2
		m_entities.push_back(p_ui);
	}
	p_ui = new UI(3, p_tower); //bossbarholder
	m_entities.push_back(p_ui);

	p_ui = new UI(4, p_tower); //instructions
	m_entities.push_back(p_ui);

	p_enemyspawner = new EnemySpawner(p_tower, p_enemy);
	m_entities.push_back(p_enemyspawner);

	p_boss = new Boss(p_tower, p_player);
	m_entities.push_back(p_boss);
	p_boss->SetAlive(false);
}

void GameState::UpdateView()
{
	//camera / view:
	DrawManager* draw_manager = ServiceLocator<DrawManager>::GetService();
	view1->reset(sf::FloatRect(0.0f, 0.0f, 1920.0f, 1080.0f)); // position x,y storlek x,y
	view1->setRotation(0);
	view1->zoom(1.0);
	view1->setCenter(960,540);//p_player->GetSpriteCenter() f�r tilf�llet ska den vara cetrerad och inte f�lja spelaren.
	draw_manager->SetView(view1);
}

bool GameState::Update(float deltaTime)
{
		DrawManager* draw_manager = ServiceLocator<DrawManager>::GetService();

		draw_manager->GetRenderWindow()->setMouseCursorVisible(false);

		//Converting pixel coordinates to world coordinates
		sf::Vector2i pixelPos = sf::Mouse::getPosition(*draw_manager->GetRenderWindow());
		sf::Vector2f coordPos = draw_manager->GetRenderWindow()->mapPixelToCoords(pixelPos);

		m_cursorSprite.setPosition(coordPos);

		if (p_player && p_player2) // kolla om f�rem�let finns i spelet
		{
			if (p_player->GetRespawnLives() <= 0 && p_player2->GetRespawnLives() <= 0)
			{
				m_win_bool = false;
				GetNextState();
				return false;
			}
		}
		if (p_player && !p_player2) // kolla om f�rem�let finns i spelet
		{
			if (p_player->GetRespawnLives() <= 0)
			{
				m_win_bool = false;
				GetNextState();
				return false;
			}		
		}
		if (p_tower->GetBossDoorReadyBool() == true)// d�rren �r redo att �ppnas.
		{
			if (m_actions[ACTION_LEFT] || m_actions[ACTION_LEFT2])
			{
				p_tower->SetBossDoor(true);
			}
		}
		if (p_boss->IsVisible() == false && p_tower->GetBossDoorOpenBool() == true)// d�rren �ppnas och bossen skapas.
		{
			p_boss->SetAlive(true);
			p_boss->SetBossStage(0);

			if (p_tower->GetBoostBool() == true)
				p_boss->SetCurrentHealth(600);

			p_bosshpbar = new BossHpBar(p_boss);
			m_entities.push_back(p_bosshpbar);

			if (p_bossbar->GetBarType()==1) // make bossbar with bartype 1 invisble
			p_bossbar->SetVisible(false);
			if (p_bossbar->GetBarType() == 2) // make bossbar with bartype 2 invisble
			p_bossbar->SetVisible(false);
			if (p_ui->GetUIType() == 3) // make UI with UItype 3 invisble
			p_ui->SetVisible(false);
		}
		if (p_boss) // kolla om f�rem�let finns i spelet
		{
			if (p_boss->GetBossAlive() == false)
			{
				m_win_bool = true;
				GetNextState();
				return false;
			}
			//bossstages, hur bossen beeter sig i spelet
			if (p_boss->GetBossStage() == 0)
			{
				p_attack->SetAlive(false);
				p_attack2->SetAlive(false);
				p_attack3->SetAlive(false);
				p_attack4->SetAlive(false);
				p_attack5->SetAlive(false);
				p_attack6->SetAlive(false);

			}
			else if (p_boss->GetBossStage() == 1)
			{
				p_attack->SetAlive(true);
				p_attack2->SetAlive(false);
				p_attack3->SetAlive(true);
				p_attack4->SetAlive(false);
				p_attack5->SetPosition(884, 585); // �terst�ll
				p_attack5->SetAlive(true);
				p_attack6->SetPosition(1326, 585); // ny pos
				p_attack6->SetAlive(true);

			}
			else if (p_boss->GetBossStage() == 2)
			{
				p_attack->SetAlive(false);
				p_attack2->SetAlive(true);
				p_attack3->SetAlive(false);
				p_attack4->SetAlive(true);
				p_attack5->SetPosition(1547, 585); // ny pos
				p_attack5->SetAlive(true);
				p_attack6->SetPosition(1105, 585); // �terst�ll
				p_attack6->SetAlive(true);
			}
			else if (p_boss->GetBossStage() == 3)
			{
				p_attack->SetAlive(false);
				p_attack2->SetAlive(false);
				p_attack3->SetAlive(false);
				p_attack4->SetAlive(false);
				p_attack5->SetAlive(false);
				p_attack6->SetAlive(false);
			}
			else if (p_boss->GetBossStage() == 4)
			{
				p_attack->SetAlive(false);
				p_attack2->SetAlive(true);
				p_attack3->SetAlive(false);
				p_attack4->SetAlive(true);
				p_attack5->SetPosition(1547, 585); // ny pos
				p_attack5->SetAlive(true);
				p_attack6->SetPosition(1105, 585); // �terst�ll
				p_attack6->SetAlive(true);
			}
		}
		if (p_enemyspawner->GetSpawnCooldown() <= 0 && p_tower->GetBossDoorReadyBool() == false)// om b�da st�mmer, 
		{
			p_enemy = new Enemy(p_tower, p_player, 1920, p_random->GetRandomIntBetween(300,680));
			m_entities.push_back(p_enemy);
		}

		//udate players to even do actions when dead.
		p_player->Update(deltaTime);
		if (p_player2)
		p_player2->Update(deltaTime);
		p_boss->Update(deltaTime);

		if (p_boss) // om bossen har skapats, uppdatera attackerna
		{
			p_attack->Update(deltaTime);
			p_attack2->Update(deltaTime);
			p_attack3->Update(deltaTime);
			p_attack4->Update(deltaTime);
			p_attack5->Update(deltaTime);
			p_attack6->Update(deltaTime);
		}

		//update all Entities
		for (size_t i = 0; i < m_entities.size(); i++)
		{
			if (m_entities[i]->IsVisible())
			{
				m_entities[i]->Update(deltaTime);
			}
		}

		//Player1 keys
		if (m_actions[ACTION_UP])
		{
			p_player->Move(DIR_UP, deltaTime);
		}
		if (m_actions[ACTION_DOWN])
		{
			p_player->Move(DIR_DOWN, deltaTime);
		}
		if (m_actions[ACTION_RIGHT])
		{
			p_player->Move(DIR_RIGHT, deltaTime);
		}
		if (m_actions[ACTION_LEFT])
		{
			p_player->Move(DIR_LEFT, deltaTime);
		}
		if (!m_actions[ACTION_UP] && !m_actions[ACTION_DOWN] && !m_actions[ACTION_RIGHT] && !m_actions[ACTION_LEFT] && p_player->GetState() != STATE_DAMAGE)
		{
			p_player->Move(DIR_IDLE, deltaTime);
		}
		if (m_actions[ACTION_ATTACK1])
		{
			if (p_player->GetActiveAttack() == false && p_player->GetCurrentEnergy() >10)
			{
				p_player->SetState(STATE_ATTACK);
				p_player->ActivateAttack();
			}
		}

		//Player2 keys
		if (p_player2)
		{
			if (m_actions[ACTION_UP2])
			{
				p_player2->Move(DIR_UP, deltaTime);
			}
			if (m_actions[ACTION_DOWN2])
			{
				p_player2->Move(DIR_DOWN, deltaTime);
			}
			if (m_actions[ACTION_RIGHT2])
			{
				p_player2->Move(DIR_RIGHT, deltaTime);
			}
			if (m_actions[ACTION_LEFT2])
			{
				p_player2->Move(DIR_LEFT, deltaTime);
			}
			if (!m_actions[ACTION_UP2] && !m_actions[ACTION_DOWN2] && !m_actions[ACTION_RIGHT2] && !m_actions[ACTION_LEFT2] && p_player2->GetState() != STATE_DAMAGE)
			{
				p_player2->Move(DIR_IDLE, deltaTime);
			}
			if (m_actions[ACTION_ATTACK12])
			{
				if (p_player2->GetActiveAttack() == false && p_player2->GetCurrentEnergy() > 10)
				{
					p_player2->SetState(STATE_ATTACK);
					p_player2->ActivateAttack();
				}
			}
		}

		// Show gold
		text.setPosition(200, 10);
		//text.setPosition(draw_manager->GetRenderWindow()->mapPixelToCoords(sf::Vector2i(draw_manager->GetRenderWindow()->getSize().x/1.3, 1)));
		if (p_player && p_player2)
		text.setString("Gold: " + std::to_string(p_player->GetGold() + p_player2->GetGold()));
		if (!p_player2 && p_player)
		text.setString("Gold: " + std::to_string(p_player->GetGold()));
		
		CollisionCheck();

		TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();

		float overlapX = 0, overlapY = 0;

		//-- Update Funtions -- //
		UpdateView();
		Draw();

	return m_state_bool; // Return "true"
}

void GameState::Draw()
{
	DrawManager* draw_manager = ServiceLocator<DrawManager>::GetService();

	//Allt som ska ritas ut i GameState

	//Entities
	for (size_t i = 0; i < m_entities.size(); i++)
	{
		if (m_entities[i]->IsVisible())
		{
			draw_manager->Draw(m_entities[i]->GetSprite(), sf::RenderStates::Default);
		}
	}

	draw_manager->Draw(m_cursorSprite, sf::RenderStates::Default);

	// draw player gold.
	draw_manager->Draw(text, sf::RenderStates::Default);
}

std::string GameState::GetNextState()
{
	DrawManager* draw_manager = ServiceLocator<DrawManager>::GetService();
	view1->reset(sf::FloatRect(0, 0, 1920, 1080)); // position x,y storlek x,y
	view1->setRotation(0);
	view1->zoom(1.0);
	draw_manager->SetView(view1);

	return "MenuState";
}

void GameState::OnAction(const std::string& action, bool state)
{
// note(tommi): map actions to an internal structure 
	//player1
	if (action.compare(Action::LEFT) == 0)
	{
		m_actions[ACTION_LEFT] = state;
	}
	else if (action.compare(Action::RIGHT) == 0)
	{
		m_actions[ACTION_RIGHT] = state;
	}
	else if (action.compare(Action::UP) == 0)
	{
		m_actions[ACTION_UP] = state;
	}
	else if (action.compare(Action::DOWN) == 0)
	{
		m_actions[ACTION_DOWN] = state;
	}
	else if (action.compare(Action::ATTACK1) == 0)
	{
		m_actions[ACTION_ATTACK1] = state;
	}
	else if (action.compare(Action::MENU) == 0)
	{
		m_actions[ACTION_MENU] = state;
	}
	else if (action.compare(Action::SELECT3) == 0)
	{
		m_actions[ACTION_SELECT3] = state;
	}
	else if (action.compare(Action::SELECT4) == 0)
	{
		m_actions[ACTION_SELECT4] = state;
	}
	//player2
	if (p_player2)
	{
		if (action.compare(Action::LEFT2) == 0)
		{
			m_actions[ACTION_LEFT2] = state;
		}
		else if (action.compare(Action::RIGHT2) == 0)
		{
			m_actions[ACTION_RIGHT2] = state;
		}
		else if (action.compare(Action::UP2) == 0)
		{
			m_actions[ACTION_UP2] = state;
		}
		else if (action.compare(Action::DOWN2) == 0)
		{
			m_actions[ACTION_DOWN2] = state;
		}
		else if (action.compare(Action::ATTACK12) == 0)
		{
			m_actions[ACTION_ATTACK12] = state;
		}
		else if (action.compare(Action::MENU2) == 0)
		{
			m_actions[ACTION_MENU2] = state;
		}
		else if (action.compare(Action::SELECT32) == 0)
		{
			m_actions[ACTION_SELECT32] = state;
		}
		else if (action.compare(Action::SELECT42) == 0)
		{
			m_actions[ACTION_SELECT42] = state;
		}
	}

	
}

void GameState::CollisionCheck() // ska fixa collisions vid senare tilf�lle
{
	float overlapX = 0, overlapY = 0;

	for (int i = 0; i < m_entities.size(); i++)
	{
		if (m_entities[i]->GetCollider() == nullptr)
		{
			std::cout << "No Collider!";
		}
		Enemy* enemy = static_cast<Enemy*>(m_entities[i]);
		Player* player = static_cast<Player*>(m_entities[i]);

		if (m_entities[i]->GetType() == ENTITY_ENEMY)
		{
			if (enemy->IsVisible() == true)
			{
				if (CollisionManager::Check(p_player->GetCollider(), enemy->GetCollider(), overlapX, overlapY) && p_player->GetActiveAttack() == false)//abs(p_player->GetSpriteCenter().x - enemy->GetPos().x) < 128 && abs(p_player->GetSpriteCenter().y - enemy->GetPos().y) < 128)
				{
					if (enemy->GetAttacking() == true) // if enemy attacks, reduce health on player
					p_player->ReduceHealth(1);
				}
				if (CollisionManager::Check(p_player->GetCollider(), enemy->GetAttackCollider(), overlapX, overlapY))//abs(p_player->GetSpriteCenter().x - enemy->GetPos().x) < 128 && abs(p_player->GetSpriteCenter().y - enemy->GetPos().y) < 128)
				{
					if (enemy->GetAttacking() == false)
					{
						enemy->ActivateEAttacking();
					}
				}
				if (p_player2)
				{
					if (CollisionManager::Check(p_player2->GetCollider(), enemy->GetCollider(), overlapX, overlapY) && p_player2->GetActiveAttack() == false)//abs(p_player->GetSpriteCenter().x - enemy->GetPos().x) < 128 && abs(p_player->GetSpriteCenter().y - enemy->GetPos().y) < 128)
					{
						if (enemy->GetAttacking() == true)// if enemy attacks, reduce health on player
						p_player2->ReduceHealth(1);
					}
					if (CollisionManager::Check(p_player2->GetCollider(), enemy->GetAttackCollider(), overlapX, overlapY))//abs(p_player->GetSpriteCenter().x - enemy->GetPos().x) < 128 && abs(p_player->GetSpriteCenter().y - enemy->GetPos().y) < 128)
					{
						if (enemy->GetAttacking() == false)
						{
							enemy->ActivateEAttacking();
						}
					}
				}
			
			}

			if (enemy->GetCurrentHealth()<=0 && enemy->IsVisible() == true && p_tower->GetBossDoorOpenBool() == false) // fungerar men ingen begr�nsning och forts�tter n�r den �r d�d.
			{
				p_loot = new Loot(p_tower); 
				p_loot->SetPosition(enemy->GetX(), enemy->GetY());
				m_entities.push_back(p_loot);
			}
			if (enemy && p_tower->GetBossDoorOpenBool() == true)
			{
				p_player->ActivateBossRules();
				if (p_player2)
				p_player2->ActivateBossRules();
				enemy->KillEnemy();
			}

			// fienden ska g� mot n�rmaste spelare.
			if (abs(p_player->GetSpriteCenter().x - enemy->GetPos().x) < 300 && abs(p_player->GetSpriteCenter().y - enemy->GetPos().y) < 300)
			{
				//enemy->SetPosition(enemy->GetX() - p_player->GetX(), enemy->GetY() - p_player->GetY());
			}
		}
		if (m_entities[i]->GetType() == ENTITY_PLAYER)
		{
			for (unsigned int j = 0; j < m_entities.size(); j++)
			{
				Enemy* enemy = static_cast<Enemy*>(m_entities[j]);
				Boss* boss = static_cast<Boss*>(m_entities[j]);
				if (i != j)
				{
					if (player->IsVisible())
					{
						if (CollisionManager::Check(m_entities[j]->GetCollider(), player->GetCollider(), overlapX, overlapY))//abs(p_player->GetSpriteCenter().x - enemy->GetPos().x) < 128 && abs(p_player->GetSpriteCenter().y - enemy->GetPos().y) < 128)
						{
							if (m_entities[j]->GetType() == ENTITY_ENEMY && player->GetActiveAttack() == true)
								enemy->ReduceHealth(1);
						}
						if (CollisionManager::Check(m_entities[j]->GetCollider(), player->GetCollider(), overlapX, overlapY))//abs(p_player->GetSpriteCenter().x - enemy->GetPos().x) < 128 && abs(p_player->GetSpriteCenter().y - enemy->GetPos().y) < 128)
						{
							if (m_entities[j]->GetType() == ENTITY_BOSS && player->GetActiveAttack() == true && boss->IsInvincible()==false)
								boss->ReduceHealth(1);
						}
					}
				}
			}
		}
		if (m_entities[i]->GetType() == ENTITY_LOOT)
		{
			Loot* loot = static_cast<Loot*>(m_entities[i]);
			if (CollisionManager::Check(p_player->GetCollider(), loot->GetCollider(), overlapX, overlapY)) //CollisionManager::Check(CollisionManager::Check p_player->GetCollider(), m_entities[i]->GetCollider(), overlapX, overlapY))
			{
				if (loot->GetLooted() == false)
				{
					loot->SetLooted(true);
					if (loot->GetLooted() == true)
					{
						p_loot->SetLootSound("../assets/sounds/button.wav");
						p_loot->PlayLootSound();
						if (loot->GetLootType() == LOOT_GOLD)
						{
							p_player->IncreaseGold(10);// *gettower); // 10 * gettower, f�r att bel�ningen ska bli st�rre beroende p� vilken sv�righetsgrad det �r.
							//printf("GoldLooted");
						}
						if (loot->GetLootType() == LOOT_CRYSTAL)
						{
							p_player->IncreaseRespawnLives(1);// *gettower);
							//printf("CrystalLooted");
						}
						if (loot->GetLootType() == LOOT_HPPOT)
						{
							p_player->IncreaseHealth(30);// *gettower);
							//printf("HpPotLooted");
						}
						if (loot->GetLootType() == LOOT_ENERGYPOT)
						{
							p_player->IncreaseEnergy(10);// *gettower);
							//printf("EnergyPotLooted");
						}
					}
				}
			}
			if (p_player2)
			{
				if (CollisionManager::Check(p_player2->GetCollider(), loot->GetCollider(), overlapX, overlapY)) //CollisionManager::Check(CollisionManager::Check p_player->GetCollider(), m_entities[i]->GetCollider(), overlapX, overlapY))
				{
					if (loot->GetLooted() == false)
					{
						p_loot->SetLootSound("../assets/sounds/button.wav");
						p_loot->PlayLootSound();
						loot->SetLooted(true);
						if (loot->GetLooted() == true)
						{
							if (loot->GetLootType() == LOOT_GOLD)
							{
								p_player2->IncreaseGold(10);// *gettower);
							}
							if (loot->GetLootType() == LOOT_CRYSTAL)
							{
								p_player2->IncreaseRespawnLives(1);// *gettower);
							}
							if (loot->GetLootType() == LOOT_HPPOT)
							{
								p_player2->IncreaseHealth(30);// *gettower);
							}
							if (loot->GetLootType() == LOOT_ENERGYPOT)
							{
								p_player2->IncreaseEnergy(10);// *gettower);
							}
						}
					}
				}
			}
		}
		if (m_entities[i]->GetType() == ENTITY_ATTACK)
		{
			Attack* attack = static_cast<Attack*>(m_entities[i]);
			for (unsigned int j = 0; j < m_entities.size(); j++)
			{
				Player* player = static_cast<Player*>(m_entities[j]);
				if (i != j)
				{
					if (attack->IsVisible())
					{
						if (CollisionManager::Check(m_entities[j]->GetCollider(), attack->GetCollider(), overlapX, overlapY))//abs(p_player->GetSpriteCenter().x - enemy->GetPos().x) < 128 && abs(p_player->GetSpriteCenter().y - enemy->GetPos().y) < 128)
						{
							if (m_entities[j]->GetType() == ENTITY_PLAYER && attack->GetDangerous() == true)
							player->ReduceHealth(1);
						}
					}
				}
			}
		}
		if (m_entities[i]->GetType() == ENTITY_TOWER)
		{
			Tower* tower = static_cast<Tower*>(m_entities[i]);
			
					if (CollisionManager::Check(tower->GetCollider(), p_player->GetCollider(), overlapX, overlapY) || p_player2 && CollisionManager::Check(p_player2->GetCollider(), tower->GetCollider(), overlapX, overlapY))//abs(p_player->GetSpriteCenter().x - enemy->GetPos().x) < 128 && abs(p_player->GetSpriteCenter().y - enemy->GetPos().y) < 128)
					{
						tower->SetMove(true);
					}
					else
					{
						tower->SetMove(false);
					}
		}
	}//for(i)
}

bool GameState::GetWinBool()
{
	return m_win_bool;
}
