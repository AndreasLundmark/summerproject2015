#include "stdafx.h"
#include "ServiceLocator.hpp"
#include "TextureManager.hpp"
#include "DrawManager.hpp"
#include "AudioManager.hpp"

#include "Collider.h"
#include "ShopItem.h"

ShopItem::ShopItem(int itemType, int x, int y)
{
	m_itemType = (ItemType)itemType;

	TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();

	//Set sprite
	m_ShopItemSprite.setTexture(*texture_manager->LoadTexture("../assets/shopitems.png"));
	m_ShopItemSprite.setTextureRect(sf::IntRect(0, 0, 404, 462));
	m_ShopItemSprite.setPosition(x, y);

	p_collider = new Collider();
	p_collider->SetWidthHeight(m_ShopItemSprite.getTextureRect().width /2, 231);
	p_collider->SetPosition(m_ShopItemSprite.getPosition().x, m_ShopItemSprite.getPosition().y);

	//Set visible
	m_visible = true;
	//hover object
	m_hover = false;
	//knapp tryckning
	m_buttonpress = false;
}


ShopItem::~ShopItem()
{
	if (p_collider)
	{
		delete p_collider;
		p_collider = nullptr;
	}
}

void ShopItem::Update(float deltaTime)
{
	m_x = m_ShopItemSprite.getPosition().x;
	m_y = m_ShopItemSprite.getPosition().y;

	p_collider->SetPosition(m_ShopItemSprite.getPosition().x + 30, m_ShopItemSprite.getPosition().y + 30);

	if (m_hover == true)
	{
		m_ShopItemSprite.setTextureRect(sf::IntRect(0, m_itemType * 231, 202, 231));
	}
	if (m_hover == false)
	{
		m_ShopItemSprite.setTextureRect(sf::IntRect(202, m_itemType * 231, 202, 231));
	}
}

sf::Sprite ShopItem::GetSprite()
{
	return m_ShopItemSprite;
}

Collider* ShopItem::GetCollider()
{
	return p_collider;
}

float ShopItem::GetX()
{
	return m_ShopItemSprite.getPosition().x;
}

float ShopItem::GetY()
{
	return m_ShopItemSprite.getPosition().y;
}

sf::Vector2f ShopItem::GetPos()
{
	return m_ShopItemSprite.getPosition();
}

bool ShopItem::IsVisible()
{
	return m_visible;
}

void ShopItem::SetVisible(bool mode)
{
	m_visible = mode;
}

void ShopItem::Hover(bool mode)
{
	m_hover = mode;
}

bool ShopItem::GetHover()
{
	return m_hover;
}

EEntityType ShopItem::GetType()
{
	return ENTITY_SHOPITEM;
}

ItemType  ShopItem::GetItemType()
{
	return m_itemType;
}

void ShopItem::SetPosition(float x, float y)
{
	m_ShopItemSprite.setPosition(x, y);
}