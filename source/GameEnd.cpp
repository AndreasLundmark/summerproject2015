//GameEnd.cpp

// hann inte implementera den h�r i inl�mmnings versionen, kommer att implementeras senare.

#include "stdafx.h"
#include "ServiceLocator.hpp"
#include "TextureManager.hpp"
#include "DrawManager.hpp"
#include "Collider.h"
#include "CollisionManager.hpp"
#include "StateManager.hpp"
#include "AudioManager.hpp"
#include "InputManager.hpp"

#include <iostream>

#include "AbstractState.hpp"
#include "GameState.h"
#include "MenuButton.h"
#include "GameEnd.h"

GameEnd::GameEnd()
{
	DrawManager* draw_manager = ServiceLocator<DrawManager>::GetService();
	TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();
	AudioManager* audio_manager = ServiceLocator<AudioManager>::GetService();

	m_menuMode = MODE_LOST;

	m_button = new MenuButton(5, 100, 763);
	m_button->SetVisible(false);
	v_menuButtons.push_back(m_button);

	//std::string menupic = ("../assets/Main menu.png"); // menupic �ndras i Menustate.h!!!!

	m_backgroround.setTexture(*texture_manager->LoadTexture(menupic));

	intro = audio_manager->LoadMusicFromFile("../assets/sounds/menu1.wav");
	intro->setLoop(true);
	intro->setVolume(50);

	shop = audio_manager->LoadMusicFromFile("../assets/sounds/happytown.wav");
	shop->setLoop(true);
	shop->setVolume(50);

	button = audio_manager->LoadMusicFromFile("../assets/sounds/button.wav");
	button->setLoop(false);
	button->setVolume(30);

	if (!font.loadFromFile("../assets/OpenSans-Bold.ttf"))
	{

	}
	// Create a text
	text.setFont(font);
	text.setCharacterSize(35);
	text.setStyle(sf::Text::Bold);
	text.setColor(sf::Color(200, 200, 100, 255));

	m_menuView = new sf::View();
	m_menuView->reset(sf::FloatRect(0, 0, 1920, 1080)); // position x,y storlek x,y
	draw_manager->SetView(m_menuView);
}

GameEnd::~GameEnd()
{
	if (m_button)
	{
		delete m_button;
		m_button = nullptr;
	}
	if (m_mouseCollider)
	{
		delete 	m_mouseCollider;
		m_mouseCollider = nullptr;
	}
}

bool GameEnd::Enter()
{

	intro->play();
	return true;
}

void GameEnd::Exit()
{
	intro->stop();
}

bool GameEnd::Update(float deltatime)
{
	TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();

	for (int i = 0; i < v_menuButtons.size(); i++)
	{
		v_menuButtons[i]->Update(deltatime);
		v_menuButtons[i]->SetVisible(false);
	}

	switch (m_menuMode)
	{
	case(MODE_WIN) :
	{
		for (int i = 0; i < v_menuButtons.size(); i++)
		{
			m_backgroround.setTexture(*texture_manager->LoadTexture(menupic));

			v_menuButtons[i]->Update(deltatime);
			if (v_menuButtons[i]->GetButtonType() == BUTTON_BACK
				)
				v_menuButtons[i]->SetVisible(true);
		}

		break;
	}
	case(MODE_LOST) :
	{
		for (int i = 0; i < v_menuButtons.size(); i++)
		{
			m_backgroround.setTexture(*texture_manager->LoadTexture(menupic));

			v_menuButtons[i]->Update(deltatime);
			if (v_menuButtons[i]->GetButtonType() == BUTTON_BACK
				)
				v_menuButtons[i]->SetVisible(true);
		}

		break;
	}
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Escape))
	{
		DrawManager* draw_manager = ServiceLocator<DrawManager>::GetService();

		draw_manager->GetRenderWindow()->close();
	}

	Draw();

	return buttons();
}

void GameEnd::Draw()
{
	DrawManager* draw_manager = ServiceLocator<DrawManager>::GetService();

	draw_manager->Draw(m_backgroround, sf::RenderStates::Default);

	for (int i = 0; i < v_menuButtons.size(); i++)
	{
		if (v_menuButtons[i]->IsVisible())
		{
			draw_manager->Draw(v_menuButtons[i]->GetSprite(), sf::RenderStates::Default);
		}
	}

	switch (m_menuMode)
	{
	case(MODE_WIN) :
	{
		break;
	}
	case(MODE_LOST) :
	{
		break;
	}
	}
}

bool GameEnd::buttons()
{
	TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();
	float overlapX = 0, overlapY = 0;

	DrawManager* draw_manager = ServiceLocator<DrawManager>::GetService();
	sf::Vector2i pixelPos = sf::Mouse::getPosition(*draw_manager->GetRenderWindow());
	sf::Vector2f coordPos = draw_manager->GetRenderWindow()->mapPixelToCoords(pixelPos);

	m_mouseCollider = new Collider; // g�r en ny Collider och placera b_startpositionen p� (0,0)
	m_mouseCollider->SetWidthHeight(5, 5); // storleken p� Collidern
	m_mouseCollider->SetPosition(coordPos.x, coordPos.y);

	for (int i = 0; i < v_menuButtons.size(); i++)
	{
		if (CollisionManager::Check(m_mouseCollider, v_menuButtons[i]->GetCollider(), overlapX, overlapY) && v_menuButtons[i]->IsVisible() == true)
		{
			v_menuButtons[i]->Hover(true);
			//button->play();
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				if (v_menuButtons[i]->GetButtonType() == BUTTON_BACK)
				{
					return false;
				}
			}
		}
		else
		{
			v_menuButtons[i]->Hover(false);
		}
	}

	return true;
}


std::string GameEnd::GetNextState()
{
	return "GameState";
}
