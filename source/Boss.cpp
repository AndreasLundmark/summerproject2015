//Enemy.cpp

#include "stdafx.h"
#include "ServiceLocator.hpp"
#include "TextureManager.hpp"
#include "DrawManager.hpp"
#include "AudioManager.hpp"
#include "Tower.h"
#include "Player.h"

#include "Boss.h"
#include "Collider.h"
#include "AnimatedSprite.h"
#include "Animation.h"

Boss::Boss(Tower* tower, Player* player)
{
	p_tower = tower;
	p_player = player;

	TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();
	m_BossSprite.setTexture(*texture_manager->LoadTexture("../assets/bosshead.png"));

	p_HitRight = new Animation("HitRight");
	p_HitRight->AddFrame(sf::IntRect(0, 0, 308, 273));
	p_HitRight->SetSpriteSheet(*texture_manager->LoadTexture("../assets/bosshead.png"));//taking damage

	p_animatedSprite = new AnimatedSprite(&m_BossSprite, p_HitRight, sf::seconds(0.2f), false, true);

	AudioManager* audio_manager = ServiceLocator<AudioManager>::GetService();

	//Starting point
	m_x = 860.0f;
	m_y = 300.0f;
	m_BossSprite.setPosition(m_x, m_y);

	//Set the players initial speed
	m_moveSpeed = 350.0f;

	m_soundTimer = 0.0f;

	//Initialize collider for the whole player sprite
	p_collider = new Collider();
	p_collider->SetWidthHeight(m_BossSprite.getTextureRect().width / 4, m_BossSprite.getTextureRect().height);

	//Initialize collider for only the "feet" part of the player sprite
	p_feetCollider = new Collider();
	p_feetCollider->SetWidthHeight(0, 0);

	//Set visible
	m_visible = true;

	m_isInvincible = false;

	m_bossalive = true;

	m_bossrage = false;

	m_bosstage = 0;
	m_stageTimer = 200.0f;

	m_MaxHealth = 1000;
	m_CurrentHealth = 1000;
}

Boss::~Boss()
{
	if (p_collider)
	{
		delete p_collider;
		p_collider = nullptr;
	}

	//Removing all the animations:
	if (p_HitLeft)
	{
		delete p_HitLeft;
		p_HitLeft = nullptr;
	}
	if (p_HitRight)
	{
		delete p_HitRight;
		p_HitRight = nullptr;
	}
	if (p_IdleRight)
	{
		delete p_IdleRight;
		p_IdleRight = nullptr;
	}
	if (p_IdleLeft)
	{
		delete p_IdleLeft;
		p_IdleLeft = nullptr;
	}
	if (p_animatedSprite)
	{
		delete p_animatedSprite;
		p_animatedSprite = nullptr;
	}
}

void Boss::Update(float deltaTime)
{
	p_animatedSprite->Update(deltaTime);

	if (m_bosstage == 0 && m_y >= 30) // bossen �r od�dlig
	{
		m_BossSprite.setPosition(m_x, m_y - 1);
		m_isInvincible = true;
	}
	if (m_bosstage == 1 && m_y >= 30) // bossen �r od�dlig
	{
		m_BossSprite.setPosition(m_x, m_y - 1);
		m_isInvincible = true;
	}
	if (m_bosstage == 2 && m_y >= 30) // bossen �r od�dlig
	{
		m_BossSprite.setPosition(m_x, m_y - 1);
		m_isInvincible = true;
	}
	if (m_bosstage == 3 && m_y < 500) // boss �ker ned s� spelaren kan sl�
	{
		m_BossSprite.setPosition(m_x, m_y + 1);
		m_isInvincible = false;
	}
	if (m_bosstage == 4 && m_y >= 30) // n�r bossen f�r nog lite liv blir den sv�rare
	{
		m_BossSprite.setPosition(m_x, m_y - 1);
		m_isInvincible = true;
	}

	if (m_CurrentHealth < m_MaxHealth / 3) // if the boss is below set health he will go in to ragemode
		m_bossrage = true;

	if (m_stageTimer >= 0.0f) // countdown
		m_stageTimer -= 1.0f ;

	if (m_stageTimer <= 0.0f && m_visible==true)
	{
		m_stageTimer = 700.0f;
		if (m_bossrage == false)
		{
			if (m_bosstage >= 0)
				m_bosstage += 1;
			if (m_bosstage >= 4)
				m_bosstage = 1;
		}
		if (m_bossrage == true)
		{
			m_bossrage = 4;
			if (m_bosstage >= 0)
				m_bosstage += 1;
			if (m_bosstage >= 5)
				m_bosstage = 3;
		}
	}

	m_x = m_BossSprite.getPosition().x;
	m_y = m_BossSprite.getPosition().y;

	p_collider->Refresh(m_x, m_y);//

	if (m_CurrentHealth < 1 && m_visible == true)
		KillBoss();
}

sf::Sprite Boss::GetSprite()
{
	return m_BossSprite;
}

Collider* Boss::GetCollider()
{
	return p_collider;
}

float Boss::GetX()
{
	return m_BossSprite.getPosition().x;
}

float Boss::GetY()
{
	return m_BossSprite.getPosition().y;
}

int Boss::GetMaxHealth()
{
	return m_MaxHealth;
}

int Boss::GetCurrentHealth()
{
	return m_CurrentHealth;
}

bool Boss::GetBossAlive()
{
	return m_bossalive;
}


sf::Vector2f Boss::GetPos()
{
	return m_BossSprite.getPosition();
}

sf::Vector2f Boss :: GetSpriteCenter()
{
	return sf::Vector2f(GetPos().x + m_BossSprite.getTextureRect().width / 2,
		GetPos().y + m_BossSprite.getTextureRect().height / 2);
}

bool Boss::IsVisible()
{
	return m_visible;
}

EEntityType Boss::GetType()
{
	return ENTITY_BOSS;
}

void Boss::SetPosition(float x, float y)
{
	m_BossSprite.setPosition(x, y);
}

bool Boss::IsInvincible()
{
	return m_isInvincible;
}

void Boss::SetAlive(bool mode)
{
	m_visible = mode;
}

AnimatedSprite* Boss::GetAnimatedSprite()
{
	return p_animatedSprite;
}

void Boss::ReduceHealth(int damage)
{
	if (m_CurrentHealth > 0)
	{
		m_CurrentHealth -= damage;
	}
}

void Boss::KillBoss()
{
	m_visible = false;
	m_bossalive = false;
}

int Boss::GetBossStage()
{
	return m_bosstage;
}

void Boss::SetCurrentHealth(int health)
{
	m_CurrentHealth = health;
}

void Boss::SetBossStage(int stage)
{
	m_bosstage = stage;
}