//Enemy.cpp

#include "stdafx.h"
#include "ServiceLocator.hpp"
#include "TextureManager.hpp"
#include "DrawManager.hpp"
#include "AudioManager.hpp"

#include "Attack.h"
#include "Collider.h"
#include "AnimatedSprite.h"
#include "Animation.h"

Attack::Attack(int x)
{
	TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();
	m_AttackSprite.setTexture(*texture_manager->LoadTexture("../assets/bossattack1anim.png"));

	p_pAttack1 = new Animation("pAttack1");
	for (int i = 0; i < 4; i++)
	{
		p_pAttack1->AddFrame(sf::IntRect(221 * i, 0, 221, 391));
	}
	p_pAttack1->SetSpriteSheet(*texture_manager->LoadTexture("../assets/bossattack1anim.png"));

	p_pAttack2 = new Animation("pAttack2");
	for (int i = 0; i < 4; i++)
	{
		p_pAttack2->AddFrame(sf::IntRect(221 * i, 0, 221, 391));
	}
	p_pAttack2->SetSpriteSheet(*texture_manager->LoadTexture("../assets/bossattack2anim.png"));

	p_animatedSprite = new AnimatedSprite(&m_AttackSprite, p_pAttack1, sf::seconds(0.2f), false, true);

	AudioManager* audio_manager = ServiceLocator<AudioManager>::GetService();

	//Starting point
	m_x = x;
	m_y = 585.0f;
	m_AttackSprite.setPosition(m_x, m_y);

	//Set the players initial speed
	m_moveSpeed = 350.0f;

	m_soundTimer = 0.0f;

	p_collider = new Collider();
	p_collider->SetWidthHeight(221,391);

	//Set visible
	m_visible = false;
	m_dangerous = false;

	m_attackTimer = 0.0f;
}

Attack::~Attack()
{
	if (p_collider)
	{
		delete p_collider;
		p_collider = nullptr;
	}

	
	if (p_animatedSprite)
	{
		delete p_animatedSprite;
		p_animatedSprite = nullptr;
	}
}

void Attack::Update(float deltaTime)
{
	p_animatedSprite->Update(deltaTime);

	if (m_attackTimer > 0)
		m_attackTimer -= 1;

	if (m_attackTimer >= 0.0f)
		m_attackTimer -= 1.0f;

	if (m_attackTimer > 300)
	{
		p_animatedSprite->setAnimation(p_pAttack2);
		m_dangerous = false;
	}
	if (m_attackTimer < 300)
	{
		p_animatedSprite->setAnimation(p_pAttack1);
		m_dangerous = true;
	}
	else if (m_attackTimer <= 0)
	{
		p_animatedSprite->setAnimation(p_pAttack2);
		m_dangerous = false;
	}


	//m_AttackSprite.setPosition(p_parent->GetX() + 120, p_parent->GetY() -40);

	m_x = m_AttackSprite.getPosition().x;
	m_y = m_AttackSprite.getPosition().y;

	p_collider->Refresh(m_x, m_y);
}

sf::Sprite Attack::GetSprite()
{
	return m_AttackSprite;
	//return p_animatedSprite->GetAnimatedSprite();
}

Collider* Attack::GetCollider()
{
	return p_collider;
}

float Attack::GetX()
{
	return m_AttackSprite.getPosition().x;
}

float Attack::GetY()
{
	return m_AttackSprite.getPosition().y;
}

sf::Vector2f Attack::GetPos()
{
	return m_AttackSprite.getPosition();
}

sf::Vector2f Attack::GetSpriteCenter()
{
	return sf::Vector2f(GetPos().x + m_AttackSprite.getTextureRect().width / 2,
		GetPos().y + m_AttackSprite.getTextureRect().height / 2);
}

bool Attack::IsVisible()
{
	return m_visible;
}

EEntityType Attack::GetType()
{
	return ENTITY_ATTACK;
}

void Attack::SetPosition(float x, float y)
{
	m_AttackSprite.setPosition(x, y);
}

void Attack::SetAlive(bool mode)
{
	if (m_attackTimer <= 0)
	{
		m_visible = mode;
		m_attackTimer = 700.0f;
	}
}

AnimatedSprite* Attack::GetAnimatedSprite()
{
	return p_animatedSprite;
}

bool Attack::GetDangerous()
{
	return m_dangerous;
}