//MenuState.cpp

#include "stdafx.h"
#include "ServiceLocator.hpp"
#include "TextureManager.hpp"
#include "DrawManager.hpp"
#include "Collider.h"
#include "CollisionManager.hpp"
#include "StateManager.hpp"
#include "AudioManager.hpp"
#include "InputManager.hpp"

#include <iostream>

#include "AbstractState.hpp"
#include "MenuState.h"
#include "GameState.h"
#include "MenuButton.h"
#include "ShopItem.h"
#include "Info.h"

MenuState::MenuState()
{
	DrawManager* draw_manager = ServiceLocator<DrawManager>::GetService();
	TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();
	AudioManager* audio_manager = ServiceLocator<AudioManager>::GetService();

	m_logotimer = 0.0f;

	m_menuMode = MODE_MAIN;

	m_button = new MenuButton(0, 710, 35);
	v_menuButtons.push_back(m_button);

	m_button = new MenuButton(1, 710, 275);
	v_menuButtons.push_back(m_button);

	m_button = new MenuButton(2, 710, 515);
	v_menuButtons.push_back(m_button);

	m_button = new MenuButton(3, 710, 763);
	v_menuButtons.push_back(m_button);

	m_button = new MenuButton(5, 100, 763);
	m_button->SetVisible(false);
	v_menuButtons.push_back(m_button);

	m_button = new MenuButton(7, 100, 763);
	m_button->SetVisible(false);
	v_menuButtons.push_back(m_button);

	m_button = new MenuButton(8, 1250, 763);
	m_button->SetVisible(false);
	v_menuButtons.push_back(m_button);

	m_button = new MenuButton(9, 710, 763);
	m_button->SetVisible(false);
	v_menuButtons.push_back(m_button);

	m_button = new MenuButton(10, 1250, 763);
	m_button->SetVisible(false);
	v_menuButtons.push_back(m_button);

	m_button = new MenuButton(11, 460, 763);
	m_button->SetVisible(false);
	v_menuButtons.push_back(m_button);

	m_button = new MenuButton(12, 1000, 763);
	m_button->SetVisible(false);
	v_menuButtons.push_back(m_button);

	m_shopItem = new ShopItem(1, 600, 100);
	m_shopItem->SetVisible(false);
	v_shopItem.push_back(m_shopItem);

	m_shopItem = new ShopItem(0, 832, 100);
	m_shopItem->SetVisible(false);
	v_shopItem.push_back(m_shopItem);

	m_shopItem = new ShopItem(0, 1064, 100);
	m_shopItem->SetVisible(false);
	v_shopItem.push_back(m_shopItem);

	m_shopItem = new ShopItem(0, 600, 360);
	m_shopItem->SetVisible(false);
	v_shopItem.push_back(m_shopItem);

	m_shopItem = new ShopItem(0, 832, 360);
	m_shopItem->SetVisible(false);
	v_shopItem.push_back(m_shopItem);

	m_shopItem = new ShopItem(0, 1064, 360);
	m_shopItem->SetVisible(false);
	v_shopItem.push_back(m_shopItem);

	m_backgroround.setTexture(*texture_manager->LoadTexture(menupic));

	intro = audio_manager->LoadMusicFromFile("../assets/sounds/menu1.wav");
	intro->setLoop(true);
	intro->setVolume(50);

	button = audio_manager->LoadMusicFromFile("../assets/sounds/button.wav");
	button->setLoop(false);
	button->setVolume(30);

	if (!font.loadFromFile("../assets/OpenSans-Bold.ttf"))
	{
		printf("can't find font");
	}
	// Create a text
	text.setFont(font);
	text.setCharacterSize(35);
	text.setStyle(sf::Text::Bold);
	text.setColor(sf::Color(92, 232, 245, 255));
	// Create a text for exit screen
	QuitText.setFont(font);
	QuitText.setCharacterSize(35);
	QuitText.setStyle(sf::Text::Bold);
	QuitText.setColor(sf::Color(92, 232, 245, 255));
	//QuitText.setPosition(100, 100);
	QuitText.setPosition(draw_manager->GetRenderWindow()->mapPixelToCoords(sf::Vector2i(draw_manager->GetRenderWindow()->getSize().x / 2 - 350, 440)));
	QuitText.setString("Do you really want to quit the game?");
	

	 m_menuView = new sf::View();
	 m_menuView->reset(sf::FloatRect(0, 0, 1920, 1080)); // position x,y storlek x,y
	 draw_manager->SetView(m_menuView);

	 // how many players it should be in the next game.
	 m_players = 1;
	 // what tower to play.
	 m_tower = 1;

	 m_shopiteminfo = 1;
}

MenuState::~MenuState()
{
	if (m_button)
	{
		delete m_button;
		m_button = nullptr;
	}
	if (m_mouseCollider)
	{
		delete 	m_mouseCollider;
		m_mouseCollider = nullptr;
	}
}

bool MenuState::Enter()
{
	m_menuMode = MODE_MAIN;
	m_playergold = p_info->ReadFile(1, "../conf/pg.txt"); // ta reda p� hur mycket guld spelarna har tilsammans
	m_shopiteminfo = 1;
	intro->play();
	return true;
}

void MenuState::Exit()
{
	intro->stop();
	p_info->GoldMenuSaveToFile(m_playergold); // sparar guldet till en speciell fil
}

bool MenuState::Update(float deltatime)
{
	TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();
	DrawManager* draw_manager = ServiceLocator<DrawManager>::GetService();

	for (int i = 0; i < v_menuButtons.size(); i++)
	{
		v_menuButtons[i]->Update(deltatime);
		v_menuButtons[i]->SetVisible(false);
	}
	for (int i = 0; i < v_shopItem.size(); i++)
	{
		v_shopItem[i]->Update(deltatime);
		v_shopItem[i]->SetVisible(false);
	}

	switch (m_menuMode)
	{
		case(MODE_MAIN):
		{
			for (int i = 0; i < v_menuButtons.size(); i++)
			{
				m_backgroround.setTexture(*texture_manager->LoadTexture(menupic));

				v_menuButtons[i]->Update(deltatime);
				if (v_menuButtons[i]->GetButtonType() == BUTTON_PLAY 
					|| v_menuButtons[i]->GetButtonType() == BUTTON_CREDITS 
					|| v_menuButtons[i]->GetButtonType() == BUTTON_SCORE
					|| v_menuButtons[i]->GetButtonType() ==BUTTON_EXIT
					)
				v_menuButtons[i]->SetVisible(true);
			}

			break;
		}
		case(MODE_LEVELSELECT):
		{
			for (int i = 0; i < v_menuButtons.size(); i++)
			{
				m_backgroround.setTexture(*texture_manager->LoadTexture("../assets/tower1.png"));

				v_menuButtons[i]->Update(deltatime);
				if ( v_menuButtons[i]->GetButtonType() == BUTTON_BACK
					|| v_menuButtons[i]->GetButtonType() == BUTTON_PLAY2
					|| v_menuButtons[i]->GetButtonType() == BUTTON_NEXT )//, n�r fler niv�er skapas l�gg d� till att man kan bl�ddra.
					v_menuButtons[i]->SetVisible(true);
			}

			break;
		}
		case(MODE_PLAYERSELECT):
		{
			for (int i = 0; i < v_menuButtons.size(); i++)
			{
				m_backgroround.setTexture(*texture_manager->LoadTexture("../assets/tower1.png"));

				v_menuButtons[i]->Update(deltatime);
				if (v_menuButtons[i]->GetButtonType() == BUTTON_PLAYER1
					|| v_menuButtons[i]->GetButtonType() == BUTTON_PLAYER2
					)
					v_menuButtons[i]->SetVisible(true);
			}

			break;
		}
		case(MODE_SHOP):
		{
			m_backgroround.setTexture(*texture_manager->LoadTexture(menupic));
			
			for (int i = 0; i < v_menuButtons.size(); i++)
			{
				v_menuButtons[i]->Update(deltatime);
				if (v_menuButtons[i]->GetButtonType() == BUTTON_BACK)
					v_menuButtons[i]->SetVisible(true);
				
			}
			for (int i = 0; i < v_shopItem.size(); i++)
			{
				v_shopItem[i]->Update(deltatime);
				if (v_shopItem[i]->GetItemType() == ITEM_1
					|| v_shopItem[i]->GetItemType() == ITEM_LOCKED)
					v_shopItem[i]->SetVisible(true);
			}
			break;
		}
		case(MODE_OPTIONS):
		{
			for (int i = 0; i < v_menuButtons.size(); i++)
			{
				m_backgroround.setTexture(*texture_manager->LoadTexture("../assets/controlls.png"));

				v_menuButtons[i]->Update(deltatime);
				if (v_menuButtons[i]->GetButtonType() == BUTTON_BACK)
					v_menuButtons[i]->SetVisible(true);
			}
			break;
		}
		case(MODE_EXIT):
		{
			m_backgroround.setTexture(*texture_manager->LoadTexture(menupic));

			for (int i = 0; i < v_menuButtons.size(); i++)
			{
				v_menuButtons[i]->Update(deltatime);
				if (v_menuButtons[i]->GetButtonType() == BUTTON_YES
					|| v_menuButtons[i]->GetButtonType() == BUTTON_NO)
					v_menuButtons[i]->SetVisible(true);
			}
			break;

		}
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Escape))
	{
		draw_manager->GetRenderWindow()->close();
	}
	
	// Show gold
	//text.setPosition(100,100);
	text.setPosition(draw_manager->GetRenderWindow()->mapPixelToCoords(sf::Vector2i(draw_manager->GetRenderWindow()->getSize().x/2 -100, 30)));
	text.setString("Gold: " + std::to_string(m_playergold));

	Draw();

	if (m_fadeEffect ==true)
	{
		m_zoomElapsedTime += deltatime;
	}

	return buttons();
}

void MenuState::Draw()
{
	DrawManager* draw_manager = ServiceLocator<DrawManager>::GetService();

	draw_manager->Draw(m_backgroround, sf::RenderStates::Default);

	for (int i = 0; i < v_menuButtons.size(); i++)
	{
		if (v_menuButtons[i]->IsVisible())
		{
			draw_manager->Draw(v_menuButtons[i]->GetSprite(), sf::RenderStates::Default);
		}
	}

	for (int i = 0; i < v_shopItem.size(); i++)
	{
		if (v_shopItem[i]->IsVisible())
		{
			draw_manager->Draw(v_shopItem[i]->GetSprite(), sf::RenderStates::Default);
		}
	}

	// draw player gold.
	if (m_menuMode == MODE_SHOP)
	draw_manager->Draw(text, sf::RenderStates::Default);
	if (m_menuMode == MODE_EXIT)
		draw_manager->Draw(QuitText, sf::RenderStates::Default);

	switch (m_menuMode)
	{
	case(MODE_MAIN) :
	{
		break;
	}
	case(MODE_SHOP) :
	{
		break;
	}
	case(MODE_OPTIONS) :
	{
		break;
	}
	case(MODE_EXIT) :
	{
		break;
	}
	}
}

bool MenuState::buttons()
{
	TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();
	float overlapX = 0, overlapY = 0;

	DrawManager* draw_manager = ServiceLocator<DrawManager>::GetService();
	sf::Vector2i pixelPos = sf::Mouse::getPosition(*draw_manager->GetRenderWindow());
	sf::Vector2f coordPos = draw_manager->GetRenderWindow()->mapPixelToCoords(pixelPos);

	m_mouseCollider = new Collider; // g�r en ny Collider och placera b_startpositionen p� (0,0)
	m_mouseCollider->SetWidthHeight(5, 5); // storleken p� Collidern
	m_mouseCollider->SetPosition(coordPos.x, coordPos.y);

	for (int i = 0; i < v_menuButtons.size(); i++)
	{
		if (CollisionManager::Check(m_mouseCollider, v_menuButtons[i]->GetCollider(), overlapX, overlapY) && v_menuButtons[i]->IsVisible() == true)
		{
			v_menuButtons[i]->Hover(true);
			//button->play();
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				if (v_menuButtons[i]->GetButtonType() == BUTTON_PLAY)
				{
					m_menuMode = MODE_PLAYERSELECT; 
				}
				else if (v_menuButtons[i]->GetButtonType() == BUTTON_NEXT)
				{
					
				}
				else if (v_menuButtons[i]->GetButtonType() == BUTTON_PLAYER1)
				{
					m_players = 1;
					p_info->SaveToFile(m_players, m_tower, m_shopiteminfo);
					m_menuMode = MODE_LEVELSELECT;
				}
				else if (v_menuButtons[i]->GetButtonType() == BUTTON_PLAYER2)
				{
					m_players = 2;
					p_info->SaveToFile(m_players, m_tower, m_shopiteminfo);
					m_menuMode = MODE_LEVELSELECT;
				}
				else if (v_menuButtons[i]->GetButtonType() == BUTTON_PLAY2)
				{
					m_fadeEffect = true;
				}
				else if (v_menuButtons[i]->GetButtonType() == BUTTON_SCORE)
				{
					m_menuMode = MODE_SHOP;
				}
				else if (v_menuButtons[i]->GetButtonType() == BUTTON_CREDITS)
				{
					m_menuMode = MODE_OPTIONS;
				}
				else if (v_menuButtons[i]->GetButtonType() == BUTTON_EXIT)
				{
					m_menuMode = MODE_EXIT;
				}
				else if (v_menuButtons[i]->GetButtonType() == BUTTON_BACK)
				{
					m_menuMode = MODE_MAIN;
				}
				else if (v_menuButtons[i]->GetButtonType() == BUTTON_YES)
				{
					draw_manager->GetRenderWindow()->close();
				}
				else if (v_menuButtons[i]->GetButtonType() == BUTTON_NO)
				{
					m_menuMode = MODE_MAIN;
				}
			}
		}
		else
		{
			v_menuButtons[i]->Hover(false);
		}
	}

	for (int i = 0; i < v_shopItem.size(); i++)
	{
		v_shopItem[i]->Hover(true);

		if (CollisionManager::Check(m_mouseCollider, v_shopItem[i]->GetCollider(), overlapX, overlapY) && v_shopItem[i]->IsVisible() == true)
		{
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				if (v_shopItem[i]->GetItemType() == ITEM_LOCKED)
				{

				}
				else if (v_shopItem[i]->GetItemType() == ITEM_1)
				{
					if (m_shopiteminfo != 2 && m_playergold >= 100)
					{
						m_shopiteminfo = 2; // 2 = true 1 = false
						m_playergold -= 100;
					}
				}
			}
			else
			{
				v_shopItem[i]->Hover(false);
			}
		}
		if (m_shopiteminfo==2)
			v_shopItem[i]->Hover(true);
	}

	if (m_fadeEffect == true) // fade effekten fungerar inte riktigt, ska fixa sen
	{
		DrawManager* draw_manager = ServiceLocator<DrawManager>::GetService();
		int colorCode = (90 + ((0 - 90) * (m_zoomElapsedTime / 1.5)));
		if (m_zoomElapsedTime > 1.5)
		{
			m_menuView->zoom(1.0);
			m_fadeEffect = false;
			m_zoomElapsedTime = 0.0f;
			return false;
		}
	}

	return true;
}

int MenuState::GetPlayers()
{
	return m_players;
}

int MenuState::GetTower()
{
	return m_tower;
}


std::string MenuState::GetNextState()
{
	return "GameState";
}
