// Collider.cpp

#include "stdafx.h"
#include "Entity.h"
#include "Collider.h"

Collider::Collider()
{
	//m_parent = parent;
	m_hitBox.left = 0;
	m_hitBox.top = 0;
	m_hitBox.width = 0;
	m_hitBox.height = 0;
}

Collider::Collider(float x, float y, int width, int height)
{
	//m_parent = parent;
	m_hitBox.left = x;
	m_hitBox.top = y;
	m_hitBox.width = width;
	m_hitBox.height = height;
}

Collider::~Collider()
{
}

sf::FloatRect Collider::GetHitBox()
{
	return m_hitBox;
}

void Collider::SetPosition(float x, float y)
{
	m_hitBox.left = x;
	m_hitBox.top = y;
}

void Collider::SetWidthHeight(int width, int height)
{
	m_hitBox.width = width;
	m_hitBox.height = height;
}

float Collider::GetX()
{
	return m_hitBox.left;
}

float Collider::GetY()
{
	return m_hitBox.top;
}

int Collider::GetWidth()
{
	return m_hitBox.width;
}

int Collider::GetHeight()
{
	return m_hitBox.height;
}

void Collider::Refresh(float x, float y)
{
	m_hitBox.left = x;
	m_hitBox.top = y;
}
