#include "stdafx.h"
#include "Randomizer.h"

Randomizer::Randomizer()
{
}

Randomizer::~Randomizer()
{
}

unsigned int Randomizer::GetRandomIntBetween(int min, int max)
{
	srand((unsigned int)time(NULL));

	unsigned int m_randomNumberInt2 = 0;
	m_randomNumberInt2 = rand() % (max - min + 1) + min;

	return m_randomNumberInt2;
}

unsigned int Randomizer::GetRandomInt(int number)
{
	srand((unsigned int)time(NULL));

	unsigned int m_randomNumberInt = 0;
	m_randomNumberInt = rand() % number + 1;

	return m_randomNumberInt;
}
