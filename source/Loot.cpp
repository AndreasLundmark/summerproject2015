#include "stdafx.h"
#include "ServiceLocator.hpp"
#include "TextureManager.hpp"
#include "DrawManager.hpp"
#include "AudioManager.hpp"
#include "Player.h"

#include "Collider.h"
#include "Loot.h"
#include "AnimatedSprite.h"
#include "Animation.h"
#include "Randomizer.h"
#include "Tower.h"

Loot::Loot(Tower* tower)
{
	p_tower = tower;

	TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();
	DropRand();

	//Set sprite
	m_LootSprite.setTexture(*texture_manager->LoadTexture("../assets/loot1.a.png"));
	m_LootSprite.setTextureRect(sf::IntRect(0, 0, 70, 70));
	m_LootSprite.setPosition(600,800);

	p_LootAnimation = new Animation("LootAnimation");
	for (int i = 0; i < 5; i++)
	{
		p_LootAnimation->AddFrame(sf::IntRect(101 * i, m_loot_type * 111, 101, 111));
	}
	p_LootAnimation->SetSpriteSheet(*texture_manager->LoadTexture("../assets/loot1.a.png"));

	p_animatedSprite = new AnimatedSprite(&m_LootSprite, p_LootAnimation, sf::seconds(0.2f), false, true);

	p_collider = new Collider();
	p_collider->SetWidthHeight(70, 70);
	p_collider->SetPosition(m_LootSprite.getPosition().x, m_LootSprite.getPosition().y);

	//Set visible
	m_visible = true;
	//Item value
	m_itemValue = 10;
	//Item state
	m_looted = false;
	//How lon until the player can pick it up
	m_lootDelay = 50;
}

Loot::~Loot()
{
	if (p_collider)
	{
		delete p_collider;
		p_collider = nullptr;
	}
}

void Loot::Update(float deltaTime)
{
	m_x = m_LootSprite.getPosition().x;
	m_y = m_LootSprite.getPosition().y;

	p_animatedSprite->Update(deltaTime);

	p_animatedSprite->setLooped(true);
	p_animatedSprite->setFrameTime(sf::seconds(0.1f));
	p_animatedSprite->setAnimation(p_LootAnimation);

	p_animatedSprite->play();

	p_collider->SetPosition(m_LootSprite.getPosition().x, m_LootSprite.getPosition().y);

	if (m_lootDelay >= 0)
		m_lootDelay -= 1;

	if (p_tower->GetMoveBool() == true)
	{
		m_LootSprite.setPosition(m_x - 1, m_y);
	}
	if (m_y < 600) // om looten av n�gon anledning skapas ovanf�r backen, tryck ned den
		m_LootSprite.setPosition(m_x, m_y+3);

	if (m_looted == true)
	{
		m_visible = false;
	}
	else if (m_x <= -400 && m_visible == true) // om den lever en �r utanf�r sk�rmen, f�rst�r den
		m_visible = false;
}

sf::Sprite Loot::GetSprite()
{
	return m_LootSprite;
}

Collider* Loot::GetCollider()
{
	return p_collider;
}

float Loot::GetX()
{
	return m_LootSprite.getPosition().x;
}

float Loot::GetY()
{
	return m_LootSprite.getPosition().y;
}

sf::Vector2f Loot::GetPos()
{
	return m_LootSprite.getPosition();
}

bool Loot::IsVisible()
{
	return m_visible;
}

void Loot::SetVisible(bool mode)
{
	m_visible = mode;
}

EEntityType Loot::GetType()
{
	return ENTITY_LOOT;
}

void Loot::SetPosition(float x, float y)
{
	m_LootSprite.setPosition(x, y);
}

int Loot::GetItemValue()
{
	return m_itemValue;
}

bool Loot::GetLooted()
{
	return m_looted;
}

void Loot::SetLooted(bool mode)
{
	if (m_lootDelay <=0)
	m_looted = mode;
}

void Loot::SetLootSound(std::string fileName)
{

	AudioManager* audio_manager = ServiceLocator<AudioManager>::GetService();
	if (lootSound.getBuffer() != audio_manager->LoadSoundFromFile("../assets/" + fileName))
	{
		lootSound.stop();

		lootSound.setBuffer(*audio_manager->LoadSoundFromFile("../assets/" + fileName));
	}

	PlayLootSound();
}

void Loot::PlayLootSound()
{
	//Checking if the status of the soundsource is playing or not.
	if (lootSound.getStatus() != sf::SoundSource::Status::Playing)
	{
		lootSound.play();
	}
}

void Loot::DropRand()
{
	m_itemNr = p_randomloot->GetRandomInt(4);
	switch (m_itemNr)
	{
	case 1:
		m_loot_type = LOOT_GOLD;
		break;
	case 2:
		m_loot_type = LOOT_CRYSTAL;
		break;
	case 3:
		m_loot_type = LOOT_HPPOT;
		break;
	case 4:
		m_loot_type = LOOT_ENERGYPOT;
		break;
	}
}

Loot_type Loot::GetLootType()
{
	return m_loot_type;
}