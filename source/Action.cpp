// Action.cpp
//Tommi

#include "stdafx.h"
#include "Action.h"

const std::string Action::LEFT = "MoveLeft";
const std::string Action::RIGHT = "MoveRight";
const std::string Action::UP = "MoveUp";
const std::string Action::DOWN = "MoveDown";
const std::string Action::ATTACK1 = "Attack1";
const std::string Action::ATTACK2 = "Attack2";
const std::string Action::SWITCH = "Switch";
const std::string Action::ACTION = "Action";
const std::string Action::MENU = "Menu";
const std::string Action::SELECT3 = "Select3";
const std::string Action::SELECT4 = "Select4";

const std::string Action::LEFT2 = "MoveLeft2";
const std::string Action::RIGHT2 = "MoveRight2";
const std::string Action::UP2 = "MoveUp2";
const std::string Action::DOWN2 = "MoveDown2";
const std::string Action::ATTACK12 = "Attack12";
const std::string Action::ATTACK22 = "Attack22";
const std::string Action::SWITCH2 = "Switch2";
const std::string Action::ACTION2 = "Action2";
const std::string Action::MENU2 = "Menu2";
const std::string Action::SELECT32 = "Select32";
const std::string Action::SELECT42 = "Select42";
