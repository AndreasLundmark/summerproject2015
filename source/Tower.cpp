#include "stdafx.h"
#include "ServiceLocator.hpp"
#include "TextureManager.hpp"
#include "DrawManager.hpp"
#include "Player.h"
#include "Bossbar.h"

#include "AnimatedSprite.h"
#include "Animation.h"
#include "Collider.h"

#include "Tower.h"

Tower::Tower(Player* player, BossBar* bossbar)
{
	TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();
	m_sprite.setTexture(*texture_manager->LoadTexture("../assets/toweranim1.png"));

	m_bossboost = false; // om bossen hinner upp f�rst kommer boosten inte ges till spelaren och d�rf�r vara false.
	m_bossDoorReady = false; // n�r spelaren kommer till toppen sl�r den h�r om till true och spelaren kan m�ta bossen.
	m_bossDoorOpen = false; // om d�rren �r �ppen ska bossen tr�da fram.

	p_player = player;
	p_bossbar = bossbar;
	
	//Sets the sprite and animation for the Tower and should move when the player is moving forward.
	m_sprite.setTexture(*texture_manager->LoadTexture("../assets/toweranim1.png"));
	m_sprite.setTextureRect(sf::IntRect(0, 0, 1920, 1080));

	p_Towermoving = new Animation("TowerMoving");
	for (int i = 0; i < 4; i++)
	{
		p_Towermoving->AddFrame(sf::IntRect(1920* i, 0, 1920, 1080));
	}
	p_Towermoving->SetSpriteSheet(*texture_manager->LoadTexture("../assets/toweranim1.png"));

	p_TowerDoor = new Animation("TowerDoor");
	p_TowerDoor->AddFrame(sf::IntRect(0, 0, 1920, 1080));
	p_TowerDoor->AddFrame(sf::IntRect(1920, 0, 1920, 1080));
	p_TowerDoor->SetSpriteSheet(*texture_manager->LoadTexture("../assets/toweranim1.png"));

	p_TowerBoss = new Animation("TowerBoss"); // animera om tid finns
	p_TowerBoss->AddFrame(sf::IntRect(0, 0, 1920, 1080));
	p_TowerBoss->SetSpriteSheet(*texture_manager->LoadTexture("../assets/bossroom.png"));

	p_animatedSprite = new AnimatedSprite(&m_sprite, p_Towermoving, sf::seconds(0.5f), false, true);

	m_pos.x = 0.0;
	m_pos.y = 0.0;
	m_x = m_pos.x;
	m_y = m_pos.x;

	m_sprite.setPosition(m_pos);

	m_bossclimb = 0;
	m_playerclimb = 0;

	m_visible = true;

	m_move = false;

	//Initialize collider
	p_collider = new Collider();

	//Sets collider
	p_collider->SetPosition(1400, 300);
	p_collider->SetWidthHeight(50, 700);
}

Tower::~Tower()
{
	if (p_collider)
	{
		delete p_collider;
		p_collider = nullptr;
	}
}

void Tower::Update(float deltaTime)
{
	m_pos = m_sprite.getPosition();

	/*if (m_bossDoorOpen == true)
	printf("true");
	if (m_bossDoorOpen == false)
		printf("false");*/

	if (m_bossclimb < 501)
	{
		m_bossclimb += 0.03;
		//p_bossbar->SetPosition(p_bossbar->GetX(), p_bossbar->GetY() + 1);
		//printf("%d", m_bossclimb);
	}
	if (m_playerclimb < 501)
	{
		if (m_move == true && m_bossboost == false)
		{
			m_playerclimb += 0.07;//0.07
			p_animatedSprite->Update(deltaTime);
		}	
	}
	if (m_move == true && m_playerclimb > 501)
	{
		p_animatedSprite->setAnimation(p_TowerDoor);
		p_animatedSprite->setLooped(false);
		p_animatedSprite->Update(deltaTime);
		if (p_animatedSprite->GetCurrentFrame() == 1 && m_bossDoorReady == false) // Boss d�rren �r redo att �ppnas.
		{
			m_bossDoorReady = true;
		}
	}
	if (m_bossDoorOpen == true)
	{
		p_animatedSprite->setAnimation(p_TowerBoss);
		p_animatedSprite->Update(deltaTime);
	}

	// kollar vem som hinner f�rst och hindrar boosten f�r den som inte hinner f�rst upp.
	if (m_bossclimb != 505 && m_bossclimb >= 500 && m_playerclimb < 500)
	{
		m_bossboost = false;
		m_bossclimb = 505; // s�tter m_bossclimb till 505 f�r att den bara ska uppdatera en g�ng.
	}
	if (m_playerclimb != 505 && m_playerclimb >= 500 && m_bossclimb < 500)
		{
		m_bossboost = true;
		m_playerclimb = 505;
		}
}

Collider* Tower::GetCollider()
{
	return p_collider;
}

sf::Sprite Tower::GetSprite()
{
	return m_sprite;
}

float Tower::GetX()
{
	return m_pos.x;
}
float Tower::GetY()
{
	return m_pos.y;
}
float Tower::GetBossClimb()
{
	return m_bossclimb;
}
int Tower::GetPlayerClimb()
{
	return m_playerclimb;
}
sf::Vector2f Tower::GetPos()
{
	return m_pos;
}
bool Tower::IsVisible()
{
	return m_visible;
}

EEntityType Tower::GetType()
{
	return ENTITY_TOWER;
}

bool Tower::GetMoveBool()
{
	return m_move;
}

bool Tower::GetBoostBool()
{
	return m_bossboost;
}

void Tower::SetMove(bool mode)
{
	m_move = mode;
}

bool Tower::GetBossDoorReadyBool()
{
	return m_bossDoorReady;
}

bool Tower::GetBossDoorOpenBool()
{
	return m_bossDoorOpen;
}

void Tower::SetBossDoor(bool mode)
{
	m_bossDoorOpen = mode;
}

void Tower::SetVisible(bool mode)
{
	m_visible = mode;
}

void Tower::SetPosition(sf::Vector2f pos)
{
	m_sprite.setPosition(pos);
}

void Tower::SetPlayerClimb(int value)
{
	m_playerclimb = value;
}
