// StateManager.cpp

#include "stdafx.h"
#include "AbstractState.hpp"
#include "StateManager.hpp"

#include "Info.h"

#include "GameState.h"
#include "MenuState.h"
/*#include "GameOverState.h"*/

StateManager::StateManager()
{
	m_active_state = nullptr;
}

StateManager::~StateManager()
{
}

bool StateManager::Initialize()
{
	AttachState("GameState", new GameState());
	AttachState("MenuState", new MenuState);
	/*AttachState("GameOverState", new GameOverState);*/
	SetActiveState("MenuState");

	return true;
}

void StateManager::Shutdown()
{
	auto itr = m_states.begin();
	while (itr != m_states.end())
	{
		delete itr->second;
		++itr;
	}
	m_states.clear();
}

bool StateManager::Update(float deltaTime)
{
	if (!m_active_state)
		return false;

	if (!m_active_state->Update(deltaTime))
	{
		if (!SetActiveState(m_active_state->GetNextState()))
		{
			return false;
		}
	}
	return true;
}

void StateManager::Draw()
{
	if (!m_active_state)
		return;
	m_active_state->Draw();
}

void StateManager::AttachState(const std::string& name, AbstractState* state)
{
	m_states.insert(std::pair<std::string, AbstractState*>(name, state));
}

void StateManager::DetachState(const std::string& name)
{
	auto itr = m_states.find(name);
	if (itr != m_states.end())
	{
		delete itr->second;
		m_states.erase(itr);
	}
}

bool StateManager::SetActiveState(const std::string& name)
{
	if (m_active_state)
		m_active_state->Exit();
	m_active_state = nullptr;

	auto itr = m_states.find(name);
	if (itr != m_states.end())
	{
		m_active_state = itr->second;
		if (!m_active_state->Enter())
		{
			return false;
		}
	}
	else
	{
		return false;
	}
	return true;
}

AbstractState* StateManager::GetState(const std::string& name)
{
	auto itr = m_states.find(name);
	if (itr != m_states.end())
	{
		return itr->second;
	}
	return false;
}

