//Animation.cpp
//Johan

#include "stdafx.h"
#include "Animation.h"


Animation::Animation(std::string animationName)
{
	m_animationName = animationName;
}

Animation::~Animation()
{
	m_texture = nullptr;
}

void Animation::AddFrame(sf::IntRect rect)
{
	v_frames.push_back(rect);
}

void Animation::SetSpriteSheet(sf::Texture& texture)
{
	m_texture = &texture;
}

sf::Texture* Animation::GetSpriteSheet()
{
	return m_texture;
}

sf::IntRect* Animation::GetFrame(std::size_t n)
{
	return &v_frames[n];
}

std::size_t Animation::GetSize()
{
	return v_frames.size();
}

std::string Animation::GetAnimationName()
{
	return m_animationName;
}

