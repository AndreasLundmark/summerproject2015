
#include "stdafx.h"
#include "AnimatedSprite.h"
#include "Animation.h"

AnimatedSprite::AnimatedSprite(sf::Sprite* sprite, Animation* animation, sf::Time frameTime, bool paused, bool looped)
{
	p_animation = animation;
	m_frameTime = frameTime;
	m_currentTime.Zero;
	m_currentFrame = 0;
	m_isPaused = paused;
	m_isLooped = true;
	p_texture = nullptr;
	p_sprite = sprite;
}
AnimatedSprite::~AnimatedSprite()
{
	p_animation = nullptr;
	p_sprite = nullptr;
}

void AnimatedSprite::Update(float deltaTime)
{

	if (!m_isPaused && p_animation && p_sprite)
	{

		m_currentTime += sf::seconds(deltaTime);

		if (m_currentTime >= m_frameTime)
		{
			//resetta timer, men assigna resten till current timer
			m_currentTime = sf::microseconds(m_currentTime.asMicroseconds() % m_frameTime.asMicroseconds());

			//
			if (m_currentFrame + 1 < p_animation->GetSize())
			{
				m_currentFrame++;
			}
			else
			{
				//animation has ended
				m_currentFrame = 0;//set to start

				if (!m_isLooped)
				{
					m_isPaused = true;
				}
			}


			setFrame(m_currentFrame, false);

		}

	}
	p_sprite->setTexture(*p_animation->GetSpriteSheet());
	p_sprite->setTextureRect(sf::IntRect(*p_animation->GetFrame(m_currentFrame)));
}

void AnimatedSprite::setAnimation(Animation* animation)
{
	if (p_animation != animation)
	{
		stop();
		p_animation = animation;
		play();
	}
}

void AnimatedSprite::setFrameTime(sf::Time time)
{
	m_frameTime = time;
}

void AnimatedSprite::play()
{
	m_isPaused = false;
}

void AnimatedSprite::play(Animation* animation)
{

	p_animation = animation;
	m_isPaused = false;
}

void AnimatedSprite::pause()
{
	m_isPaused = true;
}

void AnimatedSprite::stop()
{
	m_isPaused = true;
	m_currentFrame = 0;
	m_currentTime.Zero;
}

void AnimatedSprite::setLooped(bool looped)
{
	m_isLooped = looped;
}

void AnimatedSprite::setColor(const sf::Color& color)
{

}

Animation* AnimatedSprite::getAnimation()
{
	return p_animation;
}

sf::FloatRect AnimatedSprite::getLocalBounds()
{
	sf::IntRect rect = *p_animation->GetFrame(m_currentFrame);

	float width = static_cast<float>(std::abs(rect.width));
	float height = static_cast<float>(std::abs(rect.height));

	return sf::FloatRect(0.f, 0.f, width, height);
}

sf::FloatRect AnimatedSprite::getGlobalBounds()
{
	//return getTransform().transformRect(getLocalBounds());
	//return p_animation->GetFrame();
	return sf::FloatRect(0, 0, 0, 0);
}

bool AnimatedSprite::isLooped() const
{
	return m_isLooped;
}

bool AnimatedSprite::isPlaying() const
{
	return !m_isPaused;
}

sf::Time AnimatedSprite::getFrameTime() const
{
	return m_frameTime;
}

void AnimatedSprite::setFrame(std::size_t newFrame, bool resetTime)
{
	m_currentFrame = newFrame;
	if (resetTime)
		m_currentTime.Zero;
}

sf::Texture* AnimatedSprite::GetTexture()
{
	return p_texture;
}

sf::Sprite* AnimatedSprite::GetAnimatedSprite()
{
	return p_sprite;
}

void AnimatedSprite::SetAnimationSprite(sf::Sprite& sprite)
{
	p_sprite = &sprite;
}


size_t AnimatedSprite::GetCurrentFrame()
{
	return m_currentFrame;
}
