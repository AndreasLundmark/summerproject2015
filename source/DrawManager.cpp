// DrawManager.cpp

#include "stdafx.h"
#include "DrawManager.hpp"

DrawManager::DrawManager(sf::RenderWindow* window)
{
	m_window = window;
	m_view = nullptr;
}

DrawManager::~DrawManager()
{
}

bool DrawManager::Initialize()
{
	return true;
}

void DrawManager::Shutdown()
{
	m_window = nullptr;
}

void DrawManager::Draw(const sf::Drawable& drawable, const sf::RenderStates& states)
{
	if (m_view)
		m_window->setView(*m_view);

	m_window->draw(drawable, states);
}

sf::View* DrawManager::GetView()
{
	if (m_view)
		return m_view;
	else
		return false;
}

void DrawManager::SetView(sf::View *x)
{
	m_view = x;
}

sf::RenderWindow* DrawManager::GetRenderWindow()
{
	return m_window;
}

