//Enemy.cpp

#include "stdafx.h"
#include "ServiceLocator.hpp"
#include "TextureManager.hpp"
#include "DrawManager.hpp"
#include "AudioManager.hpp"
#include "Tower.h"
#include "Player.h"

#include "Enemy.h"
#include "Collider.h"
#include "AnimatedSprite.h"
#include "Animation.h"

Enemy::Enemy(Tower* tower, Player* player, int x, int y)
{
	p_tower = tower;
	p_player = player;

	TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();
	m_EnemySprite.setTexture(*texture_manager->LoadTexture("../assets/eidle2.png"));

	p_WalkCycleRight = new Animation("WalkRight");
	for (int i = 0; i < 5; i++)
	{
		p_WalkCycleRight->AddFrame(sf::IntRect(451 * i, 411, 451, 411));
	}
	p_WalkCycleRight->SetSpriteSheet(*texture_manager->LoadTexture("../assets/ewalk3.png"));

	p_WalkCycleLeft = new Animation("WalkLeft");
	for (int i = 0; i < 5; i++)
	{
		p_WalkCycleLeft->AddFrame(sf::IntRect(451 * i, 411, 451, 411));
	}
	p_WalkCycleLeft->SetSpriteSheet(*texture_manager->LoadTexture("../assets/ewalk3.png"));

	p_IdleLeft = new Animation("IdleLeft");
	for (int i = 0; i < 5; i++)
	{
		p_IdleLeft->AddFrame(sf::IntRect(451 * i, 411, 451, 411));
	}
	p_IdleLeft->SetSpriteSheet(*texture_manager->LoadTexture("../assets/eidle2.png"));

	p_IdleRight = new Animation("IdleRight");
	for (int i = 0; i < 5; i++)
	{
		p_IdleRight->AddFrame(sf::IntRect(451 * i, 411, 451, 411));
	}
	p_IdleRight->SetSpriteSheet(*texture_manager->LoadTexture("../assets/eidle2.png"));

	p_HitLeft = new Animation("HitLeft");
	for (int i = 0; i < 5; i++)
	{
		p_HitLeft->AddFrame(sf::IntRect(451 * i, 411, 451, 411));
	}
	p_HitLeft->SetSpriteSheet(*texture_manager->LoadTexture("../assets/ehit1.png"));//taking damage

	p_HitRight = new Animation("HitRight");
	for (int i = 0; i < 5; i++)
	{
		p_HitRight->AddFrame(sf::IntRect(451 * i, 411, 451, 411));
	}
	p_HitRight->SetSpriteSheet(*texture_manager->LoadTexture("../assets/ehit1.png"));//taking damage

	p_AttackLeft = new Animation("AttackLeft");
	for (int i = 0; i < 6; i++)
	{
		p_AttackLeft->AddFrame(sf::IntRect(451 * i, 411, 451, 411));
	}
	p_AttackLeft->SetSpriteSheet(*texture_manager->LoadTexture("../assets/eattack1.png"));//taking damage

	p_AttackRight = new Animation("AttackRight");
	for (int i = 0; i < 6; i++)
	{
		p_AttackRight->AddFrame(sf::IntRect(451 * i, 0, 451, 411));
	}
	p_AttackRight->SetSpriteSheet(*texture_manager->LoadTexture("../assets/eattack1.png"));//taking damage

	p_animatedSprite = new AnimatedSprite(&m_EnemySprite, p_IdleLeft, sf::seconds(0.2f), false, true);

	AudioManager* audio_manager = ServiceLocator<AudioManager>::GetService();

	//Starting point
	m_x = x;
	m_y = y;
	m_EnemySprite.setPosition(m_x, m_y);

	//Set the players initial speed
	m_moveSpeed = 350.0f;

	m_soundTimer = 0.0f;

	//Initialize collider for the whole player sprite
	p_collider = new Collider();
	p_collider->SetWidthHeight(150, 200);

	//Initialize collider for only the "feet" part of the player sprite
	p_attackCollider = new Collider();
	p_attackCollider->SetWidthHeight(451, 300);

	//Set visible
	m_visible = true;

	m_isInvincible = false;

	m_takingDamage = false;
	m_damageTimer = 0.0f;

	m_attacking = false;
	m_attackTimer = 0.0f;

	m_MaxHealth = 85;
	m_CurrentHealth = 85;
}

Enemy::~Enemy()
{
	if (p_collider)
	{
		delete p_collider;
		p_collider = nullptr;
	}

	if (p_attackCollider)
	{
		delete p_collider;
		p_collider = nullptr;
	}

	//Removing all the animations:
	if (p_WalkCycleRight)
	{
		delete p_WalkCycleRight;
		p_WalkCycleRight = nullptr;
	}
	if (p_WalkCycleLeft)
	{
		delete p_WalkCycleLeft;
		p_WalkCycleLeft = nullptr;
	}
	if (p_HitLeft)
	{
		delete p_HitLeft;
		p_HitLeft = nullptr;
	}
	if (p_HitRight)
	{
		delete p_HitRight;
		p_HitRight = nullptr;
	}
	if (p_IdleRight)
	{
		delete p_IdleRight;
		p_IdleRight = nullptr;
	}
	if (p_IdleLeft)
	{
		delete p_IdleLeft;
		p_IdleLeft = nullptr;
	}
	if (p_AttackRight)
	{
		delete p_AttackRight;
		p_AttackRight = nullptr;
	}
	if (p_AttackLeft)
	{
		delete p_AttackLeft;
		p_AttackLeft = nullptr;
	}
	if (p_animatedSprite)
	{
		delete p_animatedSprite;
		p_animatedSprite = nullptr;
	}
}

void Enemy::Update(float deltaTime)
{
	p_animatedSprite->Update(deltaTime);

	m_x = m_EnemySprite.getPosition().x;
	m_y = m_EnemySprite.getPosition().y;

	p_attackCollider->Refresh(m_x, m_y - 20);//

	if (m_attacking == true)
	{
		p_collider->Refresh(m_x, m_y);//left
		//p_collider->Refresh(m_x + 150, m_y + 100);//right
		p_collider->SetWidthHeight(451, 331);
		p_animatedSprite->setAnimation(p_AttackLeft);
	}
	else
	{
		p_collider->Refresh(m_x + 160, m_y + 100);//left
		//p_collider->Refresh(m_x + 150, m_y + 100);//right
		p_collider->SetWidthHeight(150, 200);
	}
	if (m_takingDamage== true) // n�r fienden tar skada
		p_animatedSprite->setAnimation(p_HitLeft);
	if (GetAnimatedSprite()->getAnimation() == p_HitLeft) // n�r animationen slutat byt
	{
		if (!p_animatedSprite->isPlaying())
		{
			p_animatedSprite->setAnimation(p_WalkCycleLeft);
			m_takingDamage == false;
		}
	}
	if (m_attackTimer >= 1)
	{
		m_attackTimer -= 1.0;
	}
	else if (m_attackTimer <= 0 && m_attackTimer >=0)
	{
		m_attacking = false;
	}
	if (p_tower->GetMoveBool() == true && m_attacking == false)
	{
		m_EnemySprite.setPosition(m_x - 1.5, m_y);
		p_animatedSprite->setAnimation(p_WalkCycleLeft);
	}
	else if (p_tower->GetMoveBool() == false && m_x > -400 && m_attacking == false)
	{
		m_EnemySprite.setPosition(m_x - 1, m_y);
		p_animatedSprite->setAnimation(p_WalkCycleLeft);
	}

	if (m_CurrentHealth < 1 && m_visible == true) // mindre �n 1 i liv och �r synlig, d�da den
		KillEnemy();	
	else if (m_x <= -400 && m_visible == true) // om den lever en �r utanf�r sk�rmen, d�da den
		KillEnemy();
	//f�rhindra att fienden f�r �ver max liv och energi
	if (m_CurrentHealth > m_MaxHealth)
		m_CurrentHealth = m_MaxHealth;
}

sf::Sprite Enemy::GetSprite()
{
	return m_EnemySprite;
}

Collider* Enemy::GetCollider()
{
	return p_collider;
}

Collider* Enemy::GetAttackCollider()
{
	return p_attackCollider;
}

float Enemy::GetX()
{
	return m_EnemySprite.getPosition().x;
}

float Enemy::GetY()
{
	return m_EnemySprite.getPosition().y;
}

int Enemy::GetMaxHealth()
{
	return m_MaxHealth;
}

int Enemy::GetCurrentHealth()
{
	return m_CurrentHealth;
}

void Enemy::ReduceHealth(int damage)
{
	if (m_CurrentHealth > 0)
	{
		m_takingDamage = true;
		m_CurrentHealth -= damage;
	}	
}

void Enemy::IncreaseHealth(int Health)
{
	if (m_CurrentHealth<m_MaxHealth)
		m_CurrentHealth += Health;
}

void Enemy::KillEnemy()
{
	m_visible = false;
}

sf::Vector2f Enemy::GetPos()
{
	return m_EnemySprite.getPosition();
}

sf::Vector2f Enemy::GetSpriteCenter()
{
	return sf::Vector2f(GetPos().x + m_EnemySprite.getTextureRect().width / 2,
		GetPos().y + m_EnemySprite.getTextureRect().height / 2);
}

bool Enemy::IsVisible()
{
	return m_visible;
}

EEntityType Enemy::GetType()
{
	return ENTITY_ENEMY;
}

void Enemy::SetPosition(float x, float y)
{
	m_EnemySprite.setPosition(x, y);
}

bool Enemy::IsInvincible()
{
	return m_isInvincible;
}

void Enemy::ActivateEAttacking()
{
	if (m_attackTimer <= 0)
	{
		m_attacking = true;
		m_attackTimer = 60.0f;
	}
}

bool Enemy::GetAttacking()
{
	return m_attacking;
}

void Enemy::SetAlive(bool mode)
{
	m_visible = mode;
}

AnimatedSprite* Enemy::GetAnimatedSprite()
{
	return p_animatedSprite;
}
