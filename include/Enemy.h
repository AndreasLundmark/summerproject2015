#pragma once
#include "Entity.h"

/*class Inventory;
class Loot;*/
class AnimatedSprite;
class Animation;
class Tower;
class Player;

class Enemy : public Entity
{
public:
	Enemy(Tower* tower, Player* player, int x, int y);
	~Enemy();

	void Reset();

	void Update(float deltatime);

	Collider* GetCollider();
	Collider* GetAttackCollider();
	sf::Sprite GetSprite();
	float GetX();
	float GetY();
	sf::Vector2f GetPos();

	sf::Vector2f GetSpriteCenter();


	bool IsVisible();
	EEntityType GetType();

	void SetPosition(float x, float y);

	int GetMaxHealth();
	int GetCurrentHealth();

	void KillEnemy();
	void ReduceHealth(int damage);
	void IncreaseHealth(int health);

	bool IsInvincible();

	void ActivateEAttacking();
	bool GetAttacking();

	void SetAlive(bool mode);

	void SetThrowDirection(float x, float y);

	AnimatedSprite* GetAnimatedSprite();

private:
	float m_x;
	float m_y;
	float m_moveSpeed;

	float m_attackTimer;
	float m_damageTimer;
	bool m_visible;
	bool m_isInvincible;
	bool m_takingDamage;
	bool m_attacking;
	int m_score;
	float m_soundTimer;

	int m_MaxHealth;
	int m_CurrentHealth;

	std::string m_last_direction;

	sf::Vector2f GetTargetPos();
	sf::Sound footStepSound;
	sf::Sprite m_EnemySprite;

	Animation* p_WalkCycleLeft;
	Animation* p_WalkCycleRight;
	Animation* p_HitLeft;
	Animation* p_HitRight;
	Animation* p_IdleLeft;
	Animation* p_IdleRight;
	Animation* p_AttackLeft;
	Animation* p_AttackRight;

	AnimatedSprite* p_animatedSprite;

	Tower* p_tower;
	Player* p_player;

	sf::Vector2f m_moveDirection;

	Collider* p_collider;
	Collider* p_attackCollider;
};