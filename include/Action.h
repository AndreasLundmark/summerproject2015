// Action.hpp
//Tommi

#ifndef ACTION_HPP_INCLUDED
#define ACTION_HPP_INCLUDED

#include "stdafx.h"

class Action
{
public:
	static const std::string LEFT;
	static const std::string RIGHT;
	static const std::string UP;
	static const std::string DOWN;
	static const std::string ATTACK1;
	static const std::string ATTACK2;
	static const std::string SWITCH;
	static const std::string ACTION;
	static const std::string MENU;
	static const std::string SELECT3;
	static const std::string SELECT4;

	static const std::string LEFT2;
	static const std::string RIGHT2;
	static const std::string UP2;
	static const std::string DOWN2;
	static const std::string ATTACK12;
	static const std::string ATTACK22;
	static const std::string SWITCH2;
	static const std::string ACTION2;
	static const std::string MENU2;
	static const std::string SELECT32;
	static const std::string SELECT42;
};

#endif // ACTION_HPP_INCLUDED
