#pragma once
#include "stdafx.h"

class Info
{
public:
	Info();
	~Info();

	bool Initialize();

	int ReadFile(int getline, std::string file);
	void SaveToFile(int players, int tower, int shopitem);
	void GoldSaveToFile(int value);
	void GoldMenuSaveToFile(int value);

private:

	std::string line;
};