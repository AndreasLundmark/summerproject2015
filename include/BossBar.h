#pragma once

#include "Entity.h"

class Tower;

class BossBar : public Entity
{
public:
	BossBar(Player* player,Tower* tower, int bartype);
	~BossBar();

	void Update(float deltatime);

	sf::Sprite GetSprite();
	Collider* GetCollider();

	float GetX();
	float GetY();
	int GetBarType();
	sf::Vector2f GetPos();

	bool IsVisible();
	void SetVisible(bool mode);
	EEntityType GetType();

	void SetPosition(float x, float y);

private:
	float m_x;
	float m_y;

	float m_value;
	int m_bartype;

	bool m_visible;

	Player* p_player;
	Tower* p_tower;

	sf::Sprite m_BossBarSprite;
	Animation* p_BossBarAnimation;
	Collider* p_collider;

	AnimatedSprite* p_animatedSprite;
};