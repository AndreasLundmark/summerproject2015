// AbstractState.hpp

#ifndef ABSTRACTSTATE_HPP_INCLUDED
#define ABSTRACTSTATE_HPP_INCLUDED

class DrawManager;
class InputManager;
class sf::Sprite;
class AudioManager;

struct System
{
	int width;
	int height;
	DrawManager* draw_manager;
	InputManager* input_manager;
	sf::Sprite* sprite_manager;
	AudioManager* sound_manager;
};

	class AbstractState
	{
	public:
		virtual ~AbstractState();
		virtual bool Enter() = 0;
		virtual void Exit() = 0;
		virtual bool Update(float deltaTime) = 0;
		virtual void Draw() = 0;
		virtual std::string GetNextState() = 0;
	};

#endif // ABSTRACTSTATE_HPP_INCLUDED
