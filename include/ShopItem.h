#pragma once

#include "Entity.h"

enum ItemType
{
	ITEM_LOCKED,
	ITEM_1,
};

class ShopItem
{
public:
	ShopItem(int itemType, int x, int y);
	~ShopItem();

	void Update(float deltatime);

	sf::Sprite GetSprite();
	Collider* GetCollider();

	float GetX();
	float GetY();
	sf::Vector2f ShopItem::GetPos();
	bool IsVisible();
	void SetVisible(bool mode);
	void Hover(bool mode);
	bool GetHover();
	bool LightVisible();
	int GetItemValue();
	EEntityType GetType();
	bool buttonPress();
	ItemType GetItemType();

	sf::Sprite GetLightSprite();

	void SetPosition(float x, float y);

	bool IsInvincible();

private:
	float m_x;
	float m_y;

	int m_itemValue;

	bool m_visible;
	bool m_hover;
	bool m_buttonpress;

	bool m_showLight;

	sf::Sound m_ShopItemSound;

	sf::Sprite m_ShopItemSprite;
	Collider* p_collider;
	ItemType m_itemType;
};