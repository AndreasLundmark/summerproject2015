// DrawManager.hpp

#ifndef DRAWMANAGER_HPP_INCLUDED
#define DRAWMANAGER_HPP_INCLUDED

	class DrawManager
	{
		// note(tommi): making the copy constructor and 
		// assignment operator private we make the class
		// non-copyable
		DrawManager(const DrawManager&);
		DrawManager& operator=(const DrawManager&);

	public:
		DrawManager(sf::RenderWindow* window);
		~DrawManager();

		bool Initialize();
		void Shutdown();

		void Draw(const sf::Drawable& drawable, const sf::RenderStates& states);

		sf::RenderWindow* GetRenderWindow();

		sf::View* GetView();
		void SetView(sf::View *x);

	private:
		sf::RenderWindow* m_window;
		sf::View* m_view; // camera / view
	};

#endif // DRAWMANAGER_HPP_INCLUDED
