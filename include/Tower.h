#pragma once

#include "Entity.h"
#include "stdafx.h"

class BossBar;
class Player;
class Animation;

class Tower : public Entity
{
public:
	Tower(Player* player,BossBar* bossbar);
	~Tower();

	void Update(float deltaTime);

	sf::Sprite GetSprite();
	Collider* GetCollider();

	float GetX();
	float GetY();
	float GetBossClimb();
	int GetPlayerClimb();
	sf::Vector2f GetPos();
	bool IsVisible();

	EEntityType GetType();

	bool GetMoveBool();
	bool GetBoostBool();

	void SetPlayerClimb(int value);

	void SetMove(bool mode);

	bool GetBossDoorReadyBool();

	bool GetBossDoorOpenBool();

	void SetBossDoor(bool mode);

	void SetVisible(bool mode);

	void SetPosition(sf::Vector2f pos);

private:
	float m_x;
	float m_y;
	sf::Vector2f m_pos;

	float m_bossclimb;
	float m_playerclimb;

	bool m_move;
	bool m_bossboost;
	bool m_bossDoorReady;
	bool m_bossDoorOpen;

	bool m_visible;

	BossBar* p_bossbar;

	Player* p_player;
	sf::Sprite m_sprite;
	Animation* p_Towermoving;
	Animation* p_TowerDoor;
	Animation* p_TowerBoss;
	AnimatedSprite* p_animatedSprite;

	Collider* p_collider;
};
