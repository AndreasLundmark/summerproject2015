#pragma once
#include "Entity.h"

class AnimatedSprite;
class Animation;
class Tower;
class Player;

class Boss : public Entity
{
public:
	Boss(Tower* tower, Player* player);
	~Boss();

	void Reset();

	void Update(float deltatime);

	Collider* GetCollider();
	Collider* GetFeetCollider();
	sf::Sprite GetSprite();
	float GetX();
	float GetY();
	sf::Vector2f GetPos();

	sf::Vector2f GetSpriteCenter();

	void ReduceHealth(int damage);
	void KillBoss();

	bool IsVisible();
	EEntityType GetType();

	void SetPosition(float x, float y);

	int GetMaxHealth();
	int GetCurrentHealth();

	void SetCurrentHealth(int health);
	void SetBossStage(int stage);

	bool GetBossAlive();
	int GetBossStage();

	bool IsInvincible();

	void SetAlive(bool mode);

	AnimatedSprite* GetAnimatedSprite();

private:
	float m_x;
	float m_y;
	float m_moveSpeed;

	int m_bosstage;

	float m_stageTimer;
	float m_damageTimer;
	bool m_visible;
	bool m_isInvincible;
	int m_score;
	float m_soundTimer;

	int m_MaxHealth;
	int m_CurrentHealth;

	bool m_bossalive;
	bool m_bossrage;

	std::string m_last_direction;

	sf::Vector2f GetTargetPos();
	sf::Sound footStepSound;
	sf::Sprite m_BossSprite;

	Animation* p_WalkCycleLeft;
	Animation* p_WalkCycleRight;
	Animation* p_HitLeft;
	Animation* p_HitRight;
	Animation* p_IdleLeft;
	Animation* p_IdleRight;

	AnimatedSprite* p_animatedSprite;

	Tower* p_tower;
	Player* p_player;

	sf::Vector2f m_moveDirection;

	Collider* p_collider;
	Collider* p_feetCollider;
};