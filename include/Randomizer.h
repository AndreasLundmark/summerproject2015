#pragma once
#include "stdafx.h"
//#include <stdlib.h>
//#include <time.h>

class Randomizer
{
public:
	Randomizer();
	~Randomizer();

	unsigned int GetRandomIntBetween(int min, int max);
	unsigned int GetRandomInt(int number);

private:
	unsigned int m_randomNumberInt;
	unsigned int m_randomNumberInt2;
};