#pragma once

#include "Entity.h"

class Enemy;
class Randomizer;
class Tower;

enum Loot_type
{
	LOOT_GOLD,
	LOOT_CRYSTAL,
	LOOT_HPPOT,
	LOOT_ENERGYPOT
};

class Loot : public Entity
{
public:
	Loot(Tower* tower);
	~Loot();

	void Update(float deltatime);

	sf::Sprite GetSprite();
	Collider* GetCollider();

	float GetX();
	float GetY();
	sf::Vector2f GetPos();

	bool IsVisible();
	void SetVisible(bool mode);
	bool GetLooted();
	void SetLooted(bool mode);
	int GetItemValue();
	EEntityType GetType();

	void DropRand();
	Loot_type GetLootType();

	void SetLootSound(std::string fileName);
	void PlayLootSound();

	void SetPosition(float x, float y);

private:
	float m_x;
	float m_y;

	int m_itemValue;
	int m_itemNr;
	bool m_looted;

	float m_lootDelay;

	bool m_visible;

	sf::Sprite m_LootSprite;
	Animation* p_LootAnimation;
	Collider* p_collider;
	Tower* p_tower;
	Randomizer* p_randomloot;

	sf::Sound lootSound;

	Loot_type m_loot_type;

	AnimatedSprite* p_animatedSprite;
};