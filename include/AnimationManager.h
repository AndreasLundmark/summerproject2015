//AnimationManager.h

#pragma once

#include "stdafx.h"

class Animation;

class AnimationManager
{
public:
	AnimationManager();
	~AnimationManager();

	void AddAnimation();

private:

	std::vector<Animation*> v_animations;

};
