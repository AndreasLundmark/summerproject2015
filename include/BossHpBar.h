#pragma once

#include "Entity.h"

class BossHpBar : public Entity
{
public:
	BossHpBar(Boss* boss);
	~BossHpBar();

	void Update(float deltatime);

	sf::Sprite GetSprite();
	Collider* GetCollider();

	float GetX();
	float GetY();
	sf::Vector2f GetPos();

	bool IsVisible();
	void SetVisible(bool mode);
	EEntityType GetType();

	void SetLootSound(std::string fileName);
	void PlayLootSound();

	void SetPosition(float x, float y);

private:
	float m_x;
	float m_y;

	int m_value;

	bool m_visible;

	Boss* p_boss;

	sf::Sprite m_HpBarSprite;
	Animation* p_HpBarAnimation;
	Collider* p_collider;

	AnimatedSprite* p_animatedSprite;
};