#pragma once

#include "Entity.h"

class Tower;

class UI : public Entity
{
public:
	UI(int UItype, Tower* tower);
	~UI();

	void Update(float deltatime);

	sf::Sprite GetSprite();
	Collider* GetCollider();

	float GetX();
	float GetY();
	int GetUIType();
	sf::Vector2f GetPos();

	bool IsVisible();
	void SetVisible(bool mode);
	EEntityType GetType();

	void SetPosition(float x, float y);

private:
	float m_x;
	float m_y;

	int m_UItype;

	Collider* p_collider;

	Tower* p_tower;

	bool m_visible;

	sf::Sprite m_Sprite;
	sf::Sprite m_Sprite1;
};