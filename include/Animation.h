// Animation.h

#pragma once

#include "stdafx.h"

class Animation
{
public:
	Animation(std::string animationName);
	~Animation();

	void AddFrame(sf::IntRect rect);

	void SetSpriteSheet(sf::Texture& texture);
	sf::Texture* GetSpriteSheet();
	sf::IntRect* GetFrame(std::size_t n);
	std::size_t GetSize();
	std::string GetAnimationName();
	


private:
	std::vector<sf::IntRect> v_frames;
	sf::Texture* m_texture;
	std::string m_animationName;
};