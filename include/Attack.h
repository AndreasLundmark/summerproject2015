#pragma once
#include "Entity.h"

class AnimatedSprite;
class Animation;

class Attack : public Entity
{
public:
	Attack(int x);
	~Attack();

	void Reset();

	void Update(float deltatime);

	Collider* GetCollider();
	sf::Sprite GetSprite();
	float GetX();
	float GetY();
	sf::Vector2f GetPos();

	sf::Vector2f GetSpriteCenter();


	bool IsVisible();
	EEntityType GetType();

	void SetPosition(float x, float y);

	void SetAlive(bool mode);

	bool GetDangerous();

	AnimatedSprite* GetAnimatedSprite();

private:
	float m_x;
	float m_y;
	float m_moveSpeed;

	float m_attackTimer;
	float m_damageTimer;
	bool m_visible;
	bool m_isInvincible;
	bool m_dangerous;
	float m_soundTimer;

	std::string m_last_direction;

	sf::Vector2f GetTargetPos();
	sf::Sound AttackSound;
	sf::Sprite m_AttackSprite;

	Entity* p_parent;

	Animation* p_pAttack1;
	Animation* p_pAttack2;

	AnimatedSprite* p_animatedSprite;

	sf::Vector2f m_moveDirection;

	Collider* p_collider;
};