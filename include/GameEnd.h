//MenuState.h

#pragma once

#include "stdafx.h"
#include "AbstractState.hpp"

class Light;
class MenuButton;
class Collider;
class GameState;

enum MenuMode
{
	MODE_UNKNOWN,
	MODE_WIN,
	MODE_LOST
};

class GameEnd : public AbstractState
{
public:
	GameEnd();
	~GameEnd();

	bool buttons();
	virtual bool Enter();
	virtual void Exit();
	virtual bool Update(float deltatime);
	virtual void Draw();
	virtual std::string GetNextState();

private:

	sf::Sprite m_backgroround;

	std::string menupic = ("../assets/MainMenu.png");

	MenuButton* m_button;
	std::vector<MenuButton*> v_menuButtons;

	sf::Music* intro;
	sf::Music* shop;

	sf::Music* button;

	float m_logotimer;

	MenuMode m_menuMode;

	Collider* m_mouseCollider;

	GameState* p_gamestate;

	sf::Text text;
	sf::Font font;

	sf::View* m_menuView;
};