#pragma once

#include "Entity.h"

class HpBar : public Entity
{
public:
	HpBar(Player* player, int bartype);
	~HpBar();

	void Update(float deltatime);

	sf::Sprite GetSprite();
	Collider* GetCollider();

	float GetX();
	float GetY();
	sf::Vector2f GetPos();

	bool IsVisible();
	void SetVisible(bool mode);
	EEntityType GetType();

	void SetLootSound(std::string fileName);
	void PlayLootSound();

	void SetPosition(float x, float y);

private:
	float m_x;
	float m_y;

	int m_value;
	int m_bartype;

	bool m_visible;

	Player* p_player;

	sf::Sprite m_HpBarSprite;
	Animation* p_HpBarAnimation;
	Collider* p_collider;

	AnimatedSprite* p_animatedSprite;
};