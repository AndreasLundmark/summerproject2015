// GameState.h

#ifndef GAMESTATE_H_INCLUDED
#define GAMESTATE_H_INCLUDED

#include "AbstractState.hpp"

class Entity;
class Randomizer;
class Player;
class Attack;
class Enemy;
class EnemySpawner;
class Boss;
class BossHpBar;
class HpBar;
class BossBar;
class MenuButton;
class Collider;
class Tower;
class Loot;
class UI;
class Info;

enum GameMode
{
	MODE_PAUSE,
	MODE_PLAY,
};

class GameState : public AbstractState
{
public:
	GameState();
	~GameState();

	virtual bool Enter();
	virtual void Exit();
	virtual bool Update(float deltaTime);
	virtual void Draw();
	virtual std::string GetNextState();

	void OnAction(const std::string& action, bool state);

	void UpdateView();
	void CollisionCheck();

	bool m_bossActive;

	void BeginningSpawnObjects();

	bool GetWinBool();

private:
	void CollisionChecking();

	int getplayers;
	int gettower;
	int getshopitem;

	System m_systems;
	std::vector<Entity*> m_entities;

	sf::View* view1; // camera / view

	//here will be a map for sounds if necessary
	sf::Music* music;
	bool m_active;

	bool m_state_bool;

	bool m_win_bool;

	//F�r att slippa kasta hela tiden:
	Player* p_player;
	Player* p_player2;
	Enemy* p_enemy;
	Attack* p_enemyattack;
	EnemySpawner* p_enemyspawner;
	Boss* p_boss;
	BossHpBar* p_bosshpbar;
	HpBar* p_hpbar;
	BossBar* p_bossbar;
	Loot* p_loot;
	UI* p_ui;
	Attack* p_attack;
	Attack* p_attack2;
	Attack* p_attack3;
	Attack* p_attack4;
	Attack* p_attack5;
	Attack* p_attack6;

	sf::Text text;
	sf::Font font;

	Tower* p_tower;
	Randomizer* p_random;
	Info* p_info;

	Collider* m_mouseCollider;

	sf::Sprite m_cursorSprite;

	MenuButton* m_continue;
	MenuButton* m_quit;

	sf::Sprite m_pauseSprite;

	int m_scale;//Can be used for setting scale ie zoom

	enum m_mode
	{
		ACTION_LEFT,
		ACTION_RIGHT,
		ACTION_UP,
		ACTION_DOWN,
		ACTION_ATTACK1,
		ACTION_MENU,
		ACTION_SELECT3,
		ACTION_SELECT4,
		ACTION_LEFT2,
		ACTION_RIGHT2,
		ACTION_UP2,
		ACTION_DOWN2,
		ACTION_ATTACK12,
		ACTION_MENU2,
		ACTION_SELECT32,
		ACTION_SELECT42,
		ACTION_COUNT
	};
	bool m_actions[ACTION_COUNT];

};

#endif // GAMESTATE_H_INCLUDED