// CollisionManager.hpp

#ifndef COLLISIONMANAGER_HPP_INCLUDED
#define COLLISIONMANAGER_HPP_INCLUDED


	class Collider;
	class AbstractCollider;

	class CollisionManager
	{
		// note(tommi): making the copy constructor and 
		// assignment operator private we make the class
		// non-copyable
	/*	CollisionManager(const CollisionManager&);
		CollisionManager& operator=(const CollisionManager&);*/

	public:
		static bool Check(Collider* lhs, Collider* rhs, float& overlapX, float& overlapY);
		static sf::Vector2f MoveCheck(Collider* lhs, Collider* rhs, float& overlapX, float& overlapY);

	private:

	};


#endif // COLLISIONMANAGER_HPP_INCLUDED
