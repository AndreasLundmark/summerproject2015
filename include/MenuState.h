//MenuState.h

#pragma once

#include "stdafx.h"
#include "AbstractState.hpp"

class Light;
class MenuButton;
class ShopItem;
class Collider;
class Info;

enum MenuMode
{
	MODE_MAIN,
	MODE_LEVELSELECT,
	MODE_PLAYERSELECT,
	MODE_SHOP,
	MODE_OPTIONS,
	MODE_EXIT
};

class MenuState : public AbstractState
{
public:
	MenuState();
	~MenuState();

	bool buttons();
	virtual bool Enter();
	virtual void Exit();
	virtual bool Update(float deltatime);
	virtual void Draw();
	virtual std::string GetNextState();

	int GetPlayers();
	int GetTower();

private:

	sf::Sprite m_backgroround;

	std::string menupic = ("../assets/MainMenu.png");

	MenuButton* m_button;
	std::vector<MenuButton*> v_menuButtons;
	ShopItem* m_shopItem;
	std::vector<ShopItem*> v_shopItem;

	sf::Music* intro;
	sf::Music* shop;

	sf::Music* button;

	float m_logotimer;

	MenuMode m_menuMode;

	Collider* m_mouseCollider;

	Info* p_info;

	sf::Text text;
	sf::Text QuitText;
	sf::Font font;

	sf::View* m_menuView;

	bool m_fadeEffect;
	float m_zoomElapsedTime;

	int m_players;
	int m_tower;
	int m_shopiteminfo = 0;

	int m_playergold;
};
