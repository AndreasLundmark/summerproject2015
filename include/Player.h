#pragma once
#include "Entity.h"

/*class Inventory;
class Loot;*/
class AnimatedSprite;
class Animation;

enum PlayerState
{
	STATE_NORMAL,
	STATE_DAMAGE,
	STATE_PLAYERDEAD,
	STATE_ATTACK
};

enum WalkMode
{
	WALKMODE_NORMAL,
	WALKMODE_SPRINT
};

enum MoveDirection
{
	DIR_RIGHT,
	DIR_UP,
	DIR_LEFT,
	DIR_DOWN,
	DIR_IDLE,
};

class Player : public Entity
{
public:
	Player();
	~Player();

	void Update(float deltatime);

	Collider* GetCollider();
	Collider* GetFeetCollider();
	sf::Sprite GetSprite();
	float GetX();
	float GetY();
	sf::Vector2f GetPos();

	sf::Vector2f GetSpriteCenter();

	void Move(MoveDirection dir, float deltatime);

	bool IsVisible();
	EEntityType GetType();
	PlayerState GetState();

	void SetPosition(float x, float y);
	void SetState(PlayerState state);

	void SetWalkMode(WalkMode mode);

	int GetMaxHealth();
	int GetCurrentHealth();

	int GetMaxEnergy();
	int GetCurrentEnergy();

	int GetGold();


	int GetRespawnLives();
	int GetMaxRespawnLives();
	void IncreaseRespawnLives(int lives);
	bool GetActiveAttack();

	bool IsInvincible();

	void SetAlive(bool mode);

	void SetFootStep(std::string fileName);
	void PlayFootStep();

	void SetThrowDirection(float x, float y);

	void ActivateAttack();
	void ActivateBossRules();

	void KillPlayer();
	void Respawn();
	void ReduceHealth(int damage);
	void IncreaseHealth(int health);
	void ReduceEnergy(int usage);
	void IncreaseEnergy(int enrgy);
	void IncreaseGold(int gold);
	void IncreaseCrystal(int health);

	AnimatedSprite* GetAnimatedSprite();

private:
	float m_x;
	float m_y;
	float m_moveSpeed;

	WalkMode m_walkMode;

	float m_attackTimer;
	float m_damageTimer;
	float m_invinsibleTimer;
	bool m_visible;
	bool m_isInvincible;
	int m_score;
	float m_soundTimer;

	float m_deadTimer;
	int m_deadTick;

	int m_gold;
	int m_crystal;

	int m_MaxHealth;
	int m_CurrentHealth;

	int m_MaxEnergy;
	float m_CurrentEnergy;

	int m_RespawnLives;
	int m_MaxRespawnLives;
	bool m_attacking1;
	bool m_attacking2;

	bool m_bossRules;

	std::string m_last_direction;

	sf::Vector2f GetTargetPos();
	sf::Sound footStepSound;
	sf::Sprite m_playerSprite;

	Animation* p_WalkCycleLeft;
	Animation* p_WalkCycleRight;
	Animation* p_HitLeft;
	Animation* p_HitRight;
	Animation* p_IdleLeft;
	Animation* p_IdleRight;
	Animation* p_AttackRight;
	Animation* p_AttackLeft;

	AnimatedSprite* p_animatedSprite;

 	PlayerState m_state;

	sf::Vector2f m_moveDirection;
	bool m_attackDirection;

	Collider* p_collider;
};