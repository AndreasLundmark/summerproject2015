#pragma once
#include "Entity.h"

class Tower;
class Enemy;
class Randomizer;

class EnemySpawner : public Entity
{
public:
	EnemySpawner(Tower* tower, Enemy* enemy);
	~EnemySpawner();

	void Spawn();

	void Update(float deltatime);

	sf::Sprite GetSprite();
	Collider* GetCollider();
	float GetX();
	float GetY();
	sf::Vector2f GetPos();

	bool IsVisible();
	EEntityType GetType();

	void SetAlive(bool mode);

	int GetEnemyAmount();
	int GetSpawnCooldown();

private:
	float m_x;
	float m_y;

	sf::Sprite m_EnemySpawnerSprite;

	// allt som ska randomiseras n�r en ny finde skapas.
	int m_enemyAmount;
	int m_enemyType;
	int m_enemySpawnPoint;
	int m_spawnCooldown;

	bool m_visible;

	Tower* p_tower;
	Enemy* p_enemy;
	Randomizer* p_random;

	Collider* p_collider;
};