// Collider.h

#ifndef COLLIDER_H_INCLUDED
#define COLLIDER_H_INCLUDED

//class Entity;

class Collider
{
public:
	Collider();
	Collider(float x, float y, int width, int height);
	~Collider();

	//bool HasParent();
	//void SetParent(Entity* parent);
	//void SetParent(Tile* parent);
	//Entity* GetParent();

	sf::FloatRect GetHitBox();

	void SetPosition(float x, float y);
	void SetWidthHeight(int width, int height);

	float GetX();
	float GetY();
	int GetWidth();
	int GetHeight();

	void Refresh(float x, float y);

private:
	sf::FloatRect m_hitBox;
};

#endif // COLLIDER_H_INCLUDED
