#pragma once

#include "Entity.h"

enum ButtonType
{
	BUTTON_PLAY,
	BUTTON_SCORE,
	BUTTON_CREDITS,
	BUTTON_EXIT,
	BUTTON_CONTINUE,
	BUTTON_BACK,
	BUTTON_QUIT,
	BUTTON_YES,
	BUTTON_NO,
	BUTTON_NEXT,
	BUTTON_PLAY2,
	BUTTON_PLAYER1,
	BUTTON_PLAYER2,
};

class MenuButton
{
public:
	MenuButton(int buttonType, int x, int y);
	~MenuButton();

	void Update(float deltatime);

	sf::Sprite GetSprite();
	Collider* GetCollider();

	float GetX();
	float GetY();
	sf::Vector2f MenuButton::GetPos();
	bool IsVisible();
	void SetVisible(bool mode);
	void Hover(bool mode);
	bool GetHover();
	bool LightVisible();
	int GetItemValue();
	EEntityType GetType();
	bool buttonPress();
	ButtonType GetButtonType();

	sf::Sprite GetLightSprite();

	void SetPosition(float x, float y);

	bool IsInvincible();

private:
	float m_x;
	float m_y;

	int m_itemValue;

	bool m_visible;
	bool m_hover;
	bool m_buttonpress;

	bool m_showLight;

	sf::Sound m_MenuButtonSound;

	sf::Sprite m_MenuButtonSprite;
	Collider* p_collider;
	ButtonType m_buttonType;
};