// Entity.h

#ifndef ENTITY_H_INCLUDED
#define ENTITY_H_INCLUDED

enum EEntityType
{
	ENTITY_UNKNOWN,
	ENTITY_PLAYER,
	ENTITY_ITEM,
	ENTITY_TOWER,
	// add specific item types as entities here
	ENTITY_ENEMY,
	ENTITY_ENEMYSPAWNER,
	ENTITY_BOSS,
	ENTITY_KEY,
	ENTITY_LOOT,
	ENTITY_HPBAR,
	ENTITY_BOSSBAR,
	ENTITY_MENUBUTTON,
	ENTITY_SHOPITEM,
	ENTITY_ATTACK,
	ENTITY_UI,
};

//class Sprite;
class Collider;
class AnimatedSprite;

class Entity
{
public:
	virtual ~Entity() {}
	virtual void Update(float deltatime) = 0;
	virtual sf::Sprite GetSprite() = 0;
	virtual Collider* GetCollider() = 0;
	virtual float GetX() = 0;
	virtual float GetY() = 0;
	virtual sf::Vector2f GetPos() = 0;
	virtual bool IsVisible() = 0;
	virtual EEntityType GetType() = 0;
};

#endif // ENTITY_H_INCLUDED
